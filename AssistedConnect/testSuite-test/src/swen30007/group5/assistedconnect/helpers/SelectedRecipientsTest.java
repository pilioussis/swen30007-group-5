package swen30007.group5.assistedconnect.helpers;

import junit.framework.TestCase;

public class SelectedRecipientsTest extends TestCase {
	public SelectedRecipientsTest(){
		super();		
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SelectedRecipients.clearRecipients();
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
		SelectedRecipients.clearRecipients();
	}
	
	public void testGetRecipients() {
		assertTrue(SelectedRecipients.getRecipients() != null);
	}
	
	public void testAddRecipient() {
		int iterations = 10;
		
		for (int i = 0; i < iterations; i++){
			SelectedRecipients.addRecipient("0412321321" + i);
		}
		
		for (int i = 0; i < iterations; i++){
			assertTrue(SelectedRecipients.getRecipients().contains("0412321321" + i));
		}
	}
	
	public void testRemoveRecipient() {
		int iterations = 10;
		
		for (int i = 0; i < iterations; i++){
			SelectedRecipients.addRecipient("0412321321" + i);
		}
		
		SelectedRecipients.removeRecipient("0412321321" + 0);
		
		assertTrue(!SelectedRecipients.getRecipients().contains("0412321321" + 0));
	}
	
	public void testClearRecipients() {
		int iterations = 10;
		
		for (int i = 0; i < iterations; i++){
			SelectedRecipients.addRecipient("0412321321" + i);
		}
		
		SelectedRecipients.clearRecipients();
		
		assertTrue(SelectedRecipients.getRecipients().size() == 0);
	}
	
	public void testHasRecipient() {
		int iterations = 10;
		
		for (int i = 0; i < iterations; i++){
			SelectedRecipients.addRecipient("0412321321" + i);
		}
		
		for (int i = 0; i < iterations; i++){
			assertTrue(SelectedRecipients.hasRecipient("0412321321" + i));
		}
		
		assertTrue(!SelectedRecipients.hasRecipient("052342343"));
	}
	
}



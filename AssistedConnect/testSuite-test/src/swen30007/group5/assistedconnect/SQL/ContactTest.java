package swen30007.group5.assistedconnect.SQL;

import swen30007.group5.assistedconnect.SQL.Contact;
import android.util.Log;
import junit.framework.TestCase;

public class ContactTest extends TestCase {
	String name = "Hunain Sahim";
	String number = "0405436992";
	public ContactTest() {
		super();
	}
	
	
	public void testContactHomogenization() {
		Contact contact = new Contact(name, number);
		assertEquals(contact.getNumber(),"+61" + number.substring(1));
		assertEquals(contact.getNormalNumber(),number);
	}
}

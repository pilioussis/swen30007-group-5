package swen30007.group5.assistedconnect.SQL;

import java.util.List;

import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import android.database.CursorIndexOutOfBoundsException;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

public class ContactSQLTest extends InstrumentationTestCase {
	public ContactSQLTest(){
		super();		
	}
	private int iterations = 20; //number of contacts to insert into db 
	private String contactName = "Milk man";
	private String contactNumber = "12345678";
	
	@Override
	public void setUp(){	
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();	
	}
	@Override
	public void tearDown(){
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();
		
	}
	//test if a row can be inserted
	@SmallTest
	public void testCreate(){
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());
		Contact outContact;
		Contact inContact = new Contact(contactName,contactNumber);
		db.addContact(inContact,true);
		outContact = db.getContact(contactNumber, true);
		
		assertEquals(outContact.getName(), contactName);
		assertEquals(outContact.getNumber(), contactNumber);
		
		db.deleteContact(contactNumber,true);
	}
	
	//test basic insert to each table
	@SmallTest
	public void testTwoTablesSimple(){
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());

		Contact inContact1 = new Contact(contactName,contactNumber+ "1");
		Contact inContact2 = new Contact(contactName,contactNumber+ "2");
		
		db.addContact(inContact1,true);
		db.addContact(inContact2,false);

		Contact outContact1 = db.getContact(contactNumber+ "1",true);
		Contact outContact2 = db.getContact(contactNumber+ "2",false);
		
		assertEquals(outContact1.getName(), contactName);
		assertEquals(outContact1.getNumber(), contactNumber + "1");
		
		assertEquals(outContact2.getName(), contactName);
		assertEquals(outContact2.getNumber(), contactNumber + "2");
		
		
		db.deleteContact(contactNumber+ "1",true);
		db.deleteContact(contactNumber+ "2",false);
	}
	
	//test that each contact is inserted in correct table
	@SmallTest
	public void testTwoTablesSeparate(){
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());

		Contact inContact1 = new Contact(contactName,contactNumber+ "1");
		Contact inContact2 = new Contact(contactName,contactNumber+ "2");
		
		db.addContact(inContact1,true);
		db.addContact(inContact2,false);
		
		//if the contact is found, then failure
		try{
			db.getContact(contactNumber + "2",true);
			db.getContact(contactNumber + "1",false);
			fail("contact in wrong table");
		}
		//the correct exception to raise when contact deleted
		catch (CursorIndexOutOfBoundsException e) {
			assertTrue(true);
		}	

		db.deleteContact(contactNumber+ "1",true);
		db.deleteContact(contactNumber+ "2",false);
	}
	@SmallTest
	//tests if a row can be deleted
	public void testRemove(){
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());
		Contact outContact;
		Contact inContact = new Contact(contactName,contactNumber);
		db.addContact(inContact,true);
		
		outContact = db.getContact(contactNumber, true);
		assertEquals(outContact.getName(), contactName);
		assertEquals(outContact.getNumber(), contactNumber);
		

		db.deleteContact(contactNumber,true);
		
		//if the contact is found, then failure
		try{
			db.getContact(contactNumber,true);
			fail("exception not thrown");
		}
		//the correct exception to raise when contact deleted
		catch (CursorIndexOutOfBoundsException e) {
			assertTrue(true);
		}		
	}
	@SmallTest
	//tests if the db counts all the contacts successfully 
	public void testCount() {
		String numberStart = "1234567";
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());
		int count;
		Contact newContact;
		
		for(int i = 0; i< iterations; i++) {
			newContact = new Contact(contactName,numberStart + i);
			db.addContact(newContact,true);
			count = db.getContactsCount(true);
			assertEquals(count, i+1);
		}		
		for(int i = 0; i< iterations; i++) {
			db.deleteContact(numberStart + i,true);
		}	
	}
	@SmallTest
	//test if the db can return a list of all the contacts in the db
	public void testGetAll() {

		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());

		Contact newContact;
		
		for(int i = 0; i< iterations; i++) {
			newContact = new Contact(contactName,contactNumber + i);
			db.addContact(newContact,true);
		}	
		List<Contact> contacts = db.getAllContacts(true);
		
		assertEquals(contacts.size(),iterations);
		assertEquals(contacts.get(9).getNumber(),contactNumber+9);
		for(int i = 0; i < iterations; i++) {
			db.deleteContact(contactNumber + i,true);
		}
		
	}
	@SmallTest
	//tests if the contact string returned for all the contacts is accurate
	public void testGetAllStrings() {
		ContactSQL db = new ContactSQL(getInstrumentation().getTargetContext());

		Contact newContact;
		
		for(int i = 0; i< iterations; i++) {
			newContact = new Contact(contactName,contactNumber + i);
			db.addContact(newContact,true);
		}	
		
		String contactsString = db.getAllContactsString(true);
		
		int count = 0;
		for(int i = 0; i< contactsString.length();i++) {
			if(contactsString.charAt(i) == '\n'){
				count ++;
			}	
		}
		
		assertEquals(count,iterations);
		
		for(int i = 0;i<iterations; i++) {
			db.deleteContact(contactNumber+i,true);
		}
		
	}
	
}



package swen30007.group5.assistedconnect.SQL;


import java.util.List;

import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;

import android.database.CursorIndexOutOfBoundsException;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.SmallTest;




public class MessageSQLTest extends InstrumentationTestCase {
	public MessageSQLTest(){
		super();		
	}
	private static final String MESSAGE = "hello my name is john";
	private static final String CONTACT1 = "1234";
	private static final String CONTACT2 = "5678";
	private static final int INCOMING = 1;
	
	private final int    iterations = 20;
	
	@Override
	public void setUp(){
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();
		
	}
	//test if a row can be inserted
	@SmallTest
	public void testInsert(){
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		Message message = new Message(MESSAGE,CONTACT1,INCOMING);

		db.addMessage(message);
		Message outMessage = db.getMessage("1");

		assertEquals(outMessage.getMessage(), MESSAGE);
		assertEquals(outMessage.getContact(), CONTACT1);
		assertEquals(outMessage.getIncoming(), INCOMING);
		
	}
	
	public void testWhackyInputCharacters() {
		String whackyString = "Let's Dance!";
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		Message message = new Message(whackyString,CONTACT1,INCOMING);

		db.addMessage(message);
		Message outMessage = db.getMessage("1");

		assertEquals(outMessage.getMessage(), whackyString);
		assertEquals(outMessage.getContact(), CONTACT1);
		assertEquals(outMessage.getIncoming(), INCOMING);
	}
	
	public void testWhackyInputCharacters2() {
		String whackyString = "Let's dance! OMG? (no way) this is \"literally\" crazy []\\";
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		Message message = new Message(whackyString,CONTACT1,INCOMING);

		db.addMessage(message);
		Message outMessage = db.getMessage("1");

		assertEquals(outMessage.getMessage(), whackyString);
		assertEquals(outMessage.getContact(), CONTACT1);
		assertEquals(outMessage.getIncoming(), INCOMING);
	}
	
	//test if all messages can be accessed for a particular contact
	public void testGetMessagesFromContact() {
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		List<Message> messages;
		
		//insert messages from contact1
		for(int i = 0; i< iterations; i++) {
			Message message = new Message(MESSAGE + i,CONTACT1,INCOMING);

			db.addMessage(message);
		}
		
		//insert messages NOT from contact
		for(int i = 0; i<iterations; i++) {
			Message message = new Message(MESSAGE + i,CONTACT2,INCOMING);

			db.addMessage(message);
		}
		
		//check that the correct amount of messages from contact exist
		messages = db.getMessagesFromContact(CONTACT1);
		assertEquals(iterations,messages.size());
		
		//remove all messages from contact1
		for(int i = 0; i< iterations; i++) {
			assertEquals(messages.get(i).getMessage(), MESSAGE + i);
			db.deleteMessage((i+1)+"");
		}
		
		//check that all messages have been removed from contact
		messages = db.getMessagesFromContact(CONTACT1);
		assertEquals(0,messages.size());
	}
	
	//test create then delete message
	public void testDelete() {
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		Message message = new Message(MESSAGE,CONTACT1,INCOMING);

		db.addMessage(message);
		db.deleteMessage("1");
		
		try{
			db.getMessage("1");
			fail("exception not thrown");
		}
		//the correct exception to raise when message deleted
		catch (CursorIndexOutOfBoundsException e) {
			assertTrue(true);
		}			
	}
	
	//tests that the correct number is returned when counting all messages in db
	public void testAllMessageCount() {
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		
		assertEquals(0,db.getTotalMessageCount());
		for(int i = 0; i<iterations; i++) {
			Message message = new Message(MESSAGE + i,CONTACT2,INCOMING);
			db.addMessage(message);
		}
		
		assertEquals(iterations,db.getTotalMessageCount());
		for(int i = 0; i< iterations; i++) {
			db.deleteMessage((i+1)+"");
		}
		assertEquals(0,db.getTotalMessageCount());		
	}
	
	//tests if the newest message from a contact is correct
	public void testGetNewestMessage() {
		MessageSQL db = new MessageSQL(getInstrumentation().getTargetContext());
		Message newMessage;
		//populate db
		for(int i = 0; i<iterations; i++) {
			Message message1 = new Message(MESSAGE + i,CONTACT1,INCOMING);
			Message message2 = new Message(MESSAGE + i,CONTACT2,INCOMING);
			db.addMessage(message1);
			db.addMessage(message2);
		}
		
		//simple check
		newMessage = db.getNewestMessage(CONTACT1);
		assertEquals(MESSAGE + (iterations-1), newMessage.getMessage());
		
		//remove one to see if it picks the previous
		db.deleteMessage(iterations+"");
		newMessage = db.getNewestMessage(CONTACT1);	
		assertEquals(MESSAGE + (iterations-1),newMessage.getMessage());
		
		//check if new addition dosen't get assigned old messages id
		Message message = new Message(MESSAGE + 't',CONTACT1,INCOMING);
		db.addMessage(message);
		newMessage = db.getNewestMessage(CONTACT1);	
		assertEquals(MESSAGE + 't',newMessage.getMessage());
		
	}
	
	

	
	
}



package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.CustomTextActivity;
import swen30007.group5.assistedconnect.assisted.MessageSentSuccessActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

public class CustomTextActivityTest extends ActivityInstrumentationTestCase2<CustomTextActivity>  {

	private CustomTextActivity customTextActivity;
	private EditText editText;
	private Button buttonSendMessage;
	
	public CustomTextActivityTest(){
		super(CustomTextActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		customTextActivity = getActivity();
		editText = (EditText) customTextActivity.findViewById(R.id.written_message);
		buttonSendMessage = (Button) customTextActivity.findViewById(R.id.button_send);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
		assertTrue(customTextActivity != null);
	    assertTrue(editText != null);
		assertTrue(buttonSendMessage != null);
	}
	
	public void testKeyboardAutoDisplayed() {
		assertTrue(editText.hasFocus());
	}
	
	public void testSendMessage() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageSentSuccessActivity.class.getName(), null, false);
		
		customTextActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	buttonSendMessage.performClick();
			}
		});
		
		Activity messageSentSuccessActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(messageSentSuccessActivity);
		messageSentSuccessActivity.finish();
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.VoiceTextActivity;
import swen30007.group5.assistedconnect.assisted.VoiceTextCountdownActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class VoiceTextCountdownActivityTest extends ActivityInstrumentationTestCase2<VoiceTextCountdownActivity>  {

	private VoiceTextCountdownActivity voiceTextCountdownActivity;
	private TextView countdownText;
	
	public VoiceTextCountdownActivityTest(){
		super(VoiceTextCountdownActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		voiceTextCountdownActivity = getActivity();
		countdownText = (TextView) voiceTextCountdownActivity.findViewById(R.id.countdown_text);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(countdownText != null);
	}
	
	public void testCountdown() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(VoiceTextActivity.class.getName(), null, false);
		
		voiceTextCountdownActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
			}
		});
		
		Activity voiceTextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 7000);
		// next activity is opened and captured.
		assertNotNull(voiceTextActivity);
		voiceTextActivity.finish();
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.VoiceTextCountdownActivity;
import swen30007.group5.assistedconnect.assisted.VoiceTextFailureActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.TextView;

public class VoiceTextFailureActivityTest extends ActivityInstrumentationTestCase2<VoiceTextFailureActivity>  {

	private VoiceTextFailureActivity voiceTextFailureActivity;
	private TextView failurePromptText;
	private Button buttonVoiceTextRetry;
	
	public VoiceTextFailureActivityTest(){
		super(VoiceTextFailureActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		voiceTextFailureActivity = getActivity();
		failurePromptText = (TextView) voiceTextFailureActivity.findViewById(R.id.error_prompt_text);
		buttonVoiceTextRetry = (Button) voiceTextFailureActivity.findViewById(R.id.button_voice_text_retry);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
		assertTrue(voiceTextFailureActivity != null);
	    assertTrue(failurePromptText != null);
		assertTrue(buttonVoiceTextRetry != null);
	}
	
	public void testRetryMessage() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(VoiceTextCountdownActivity.class.getName(), null, false);
		
		voiceTextFailureActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	buttonVoiceTextRetry.performClick();
			}
		});
		
		Activity voiceTextCountdownActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(voiceTextCountdownActivity);
		voiceTextCountdownActivity.finish();
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

public class SelectRecipientsActivityTest extends ActivityInstrumentationTestCase2<SelectRecipientsActivity>  {

	private SelectRecipientsActivity selectRecipientsActivity;
	private Button doneButton;
	private GridView gridView;
	ContactSQL db;
	
	public SelectRecipientsActivityTest(){
		super(SelectRecipientsActivity.class);		
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Log.w("YOLO", "SETUP");
		
		db = new ContactSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();
		db.addContact(new Contact("john", "0424234324"), true);
		db.addContact(new Contact("jackson", "0424234224"), true);
		db.addContact(new Contact("jones", "0434234324"), true);
		db.addContact(new Contact("james", "0424264353"), true);
		db.addContact(new Contact("jason", "04435234324"), true);
		db.close();
		
		setActivityInitialTouchMode(false);

		selectRecipientsActivity = getActivity();
		doneButton = (Button) selectRecipientsActivity.findViewById(R.id.button_done_recipients);
		gridView = (GridView) selectRecipientsActivity.findViewById(R.id.gridview_contacts);
		
		SelectedRecipients.clearRecipients();
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		Log.w("YOLO", "TEARDOWN");
		SelectedRecipients.clearRecipients();
		
		db = new ContactSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();
		db.close();
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(selectRecipientsActivity != null);
	    assertTrue(doneButton != null);
	    assertTrue(gridView != null);
	}
	
	public void testAddContactButton() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ChangeContactsActivity.class.getName(), null, false);
		
		selectRecipientsActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {	
	    		View gridItem = gridView.getChildAt(gridView.getChildCount() - 1);
	    		gridView.performItemClick(gridItem, gridView.getChildCount() - 1, 0);
			}
		});
		
		Activity changeContactsActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 3000);
		// next activity is opened and captured.
		assertNotNull(changeContactsActivity);
		changeContactsActivity.finish();
	}
	
	public void testSelectingRecipient() {
		selectRecipientsActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	for (int i = 0; i < gridView.getChildCount() - 1; i++){
		    		View gridItem = gridView.getChildAt(i);
		    		View overlay = gridItem.findViewById(R.id.overlay);
		    		TextView number = (TextView) gridItem.findViewById(R.id.number);
		    		
		    		gridView.performItemClick(gridItem, i, 0);
		    		assertTrue(overlay.getVisibility() == View.VISIBLE);
		    		assertTrue(SelectedRecipients.hasRecipient((String) number.getText()));
		    	}
		    	
		    	for (int i = 0; i < gridView.getChildCount() - 1; i++){
		    		View gridItem = gridView.getChildAt(i);
		    		View overlay = gridItem.findViewById(R.id.overlay);
		    		TextView number = (TextView) gridItem.findViewById(R.id.number);
		    		
		    		gridView.performItemClick(gridItem, i, 0);
		    		assertTrue(overlay.getVisibility() == View.GONE);
		    		assertTrue(!SelectedRecipients.hasRecipient((String) number.getText()));
		    	}
			}
		});
	}
	
	public void testDoneButtonNoRecipients() {	
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageModeActivity.class.getName(), null, false);
		selectRecipientsActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	doneButton.performClick();	    	
			}
		});
		Activity messageModeActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNull(messageModeActivity);
	}
	
	public void testDoneButtonWithRecipients() {	
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageModeActivity.class.getName(), null, false);
		selectRecipientsActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	View gridItem = gridView.getChildAt(0);
	    		gridView.performItemClick(gridItem, 0, 0);
		    	doneButton.performClick();    	
			}
		});
		Activity messageModeActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 20000);
		// next activity is opened and captured.
		assertNotNull(messageModeActivity);
		messageModeActivity.finish();
	}
	
}

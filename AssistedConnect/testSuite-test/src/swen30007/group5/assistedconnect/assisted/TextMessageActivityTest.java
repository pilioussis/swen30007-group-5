package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

public class TextMessageActivityTest extends ActivityInstrumentationTestCase2<TextMessageActivity>  {

	private TextMessageActivity textMessageActivity;
    private Button default1Button;
    private Button default2Button;
    private Button default3Button;
    private Button default4Button;
    private Button customTextButton;
	
	public TextMessageActivityTest(){
		super(TextMessageActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		textMessageActivity = getActivity();
		
        default1Button = (Button) textMessageActivity.findViewById(R.id.button_def_1);
        default2Button = (Button) textMessageActivity.findViewById(R.id.button_def_2);
        default3Button = (Button) textMessageActivity.findViewById(R.id.button_def_3);
        default4Button = (Button) textMessageActivity.findViewById(R.id.button_def_4);
        customTextButton = (Button) textMessageActivity.findViewById(R.id.button_custom);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
		assertTrue(textMessageActivity != null);
		assertTrue(default1Button != null);
		assertTrue(default2Button != null);
		assertTrue(default3Button != null);
		assertTrue(default4Button != null);
		assertTrue(customTextButton != null);
	}
	
	public void testDefaultButton1() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageSentSuccessActivity.class.getName(), null, false);
		
		textMessageActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	default1Button.performClick();
			}
		});
		
		Activity messageSentSuccessActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(messageSentSuccessActivity);
		textMessageActivity.finish();
		messageSentSuccessActivity.finish();
	}
	
	public void testDefaultButton2() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageSentSuccessActivity.class.getName(), null, false);
		
		textMessageActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	default2Button.performClick();
			}
		});
		
		Activity messageSentSuccessActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(messageSentSuccessActivity);
		textMessageActivity.finish();
		messageSentSuccessActivity.finish();
	}
	
	public void testDefaultButton3() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageSentSuccessActivity.class.getName(), null, false);
		
		textMessageActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	default3Button.performClick();
			}
		});
		
		Activity messageSentSuccessActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(messageSentSuccessActivity);
		textMessageActivity.finish();
		messageSentSuccessActivity.finish();
	}
	
	public void testDefaultButton4() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MessageSentSuccessActivity.class.getName(), null, false);
		
		textMessageActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	default4Button.performClick();
			}
		});
		
		Activity messageSentSuccessActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(messageSentSuccessActivity);
		textMessageActivity.finish();
		messageSentSuccessActivity.finish();
	}
	
	public void testCustomButton() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(CustomTextActivity.class.getName(), null, false);
		
		textMessageActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	customTextButton.performClick();
			}
		});
		
		Activity customTextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(customTextActivity);
		textMessageActivity.finish();
		customTextActivity.finish();
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.helpers.VibratorWrapper;
import android.test.ActivityInstrumentationTestCase2;

public class AlarmSentActivityTest extends ActivityInstrumentationTestCase2<AlarmSentActivity> {

	private AlarmSentActivity alarmSentActivity;
	private VibratorWrapper v;
	
	public AlarmSentActivityTest(){
		super(AlarmSentActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

	    alarmSentActivity = getActivity();
	    v = alarmSentActivity.v;
	}
	
	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
		assertTrue(v != null);
	}
	
	public void testVibratorOn() {
		v.startVibrator();
		assertTrue(v.isVibrating());
	}
	
	public void testStopVibrator() {
		v.stopVibrator();
		assertTrue(!v.isVibrating());
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.MessageModeActivity;
import swen30007.group5.assistedconnect.assisted.TextMessageActivity;
import swen30007.group5.assistedconnect.assisted.VoiceTextCountdownActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

public class MessageModeActivityTest extends ActivityInstrumentationTestCase2<MessageModeActivity> {
	
	private MessageModeActivity mMActivity;
	private Button textButton;
	private Button voiceButton;
	
	public MessageModeActivityTest(){
		super(MessageModeActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

	    mMActivity = getActivity();
	    textButton=(Button)mMActivity.findViewById(R.id.button_text);
	    voiceButton=(Button)mMActivity.findViewById(R.id.button_voice);
	}
	
	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(textButton != null);
	    assertTrue(voiceButton != null);
	}
	
	public void testTextButton(){
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(TextMessageActivity.class.getName(), null, false);
		
		mMActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	textButton.performClick();
			}
		});
		
		Activity testMessageActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(testMessageActivity);
		testMessageActivity.finish();
	}
	public void testVoiceButton(){
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(VoiceTextCountdownActivity.class.getName(), null, false);
		
		mMActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	voiceButton.performClick();
			}
		});
		
		Activity textVoiceCountdownActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(textVoiceCountdownActivity);
		textVoiceCountdownActivity.finish();
	}
}

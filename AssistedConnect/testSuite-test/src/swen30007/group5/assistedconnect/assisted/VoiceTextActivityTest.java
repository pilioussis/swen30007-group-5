package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.VoiceTextActivity;
import swen30007.group5.assistedconnect.assisted.VoiceTextFailureActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class VoiceTextActivityTest extends ActivityInstrumentationTestCase2<VoiceTextActivity>  {

	private VoiceTextActivity voiceTextActivity;
	private TextView speakPrompt;
	
	public VoiceTextActivityTest(){
		super(VoiceTextActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		voiceTextActivity = getActivity();
		speakPrompt = (TextView) voiceTextActivity.findViewById(R.id.speak_prompt_text);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(speakPrompt != null);
	    assertTrue(voiceTextActivity.sr != null);
	}
	
	public void testSwitchToFailure() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(VoiceTextFailureActivity.class.getName(), null, false);
		
		voiceTextActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	voiceTextActivity.switchActivityNoTransition(VoiceTextFailureActivity.class);
			}
		});
		
		Activity voiceTextFailureActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(voiceTextFailureActivity);
		voiceTextFailureActivity.finish();
	}
}

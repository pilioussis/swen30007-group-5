package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.AssistedHomeActivity;
import swen30007.group5.assistedconnect.assisted.MessageSentSuccessActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.TextView;

public class MessageSentSuccessActivityTest extends ActivityInstrumentationTestCase2<MessageSentSuccessActivity>  {

	private MessageSentSuccessActivity mSentSuccessActivity;
	private TextView successPromptText;
	private Button buttonVoiceTextFinish;
	
	public MessageSentSuccessActivityTest(){
		super(MessageSentSuccessActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

		mSentSuccessActivity = getActivity();
		successPromptText = (TextView) mSentSuccessActivity.findViewById(R.id.success_prompt_text);
		buttonVoiceTextFinish = (Button) mSentSuccessActivity.findViewById(R.id.button_voice_text_finish);
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(successPromptText != null);
		assertTrue(buttonVoiceTextFinish != null);
	}
	
	public void testFinishMessage() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(AssistedHomeActivity.class.getName(), null, false);
		
		mSentSuccessActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	buttonVoiceTextFinish.performClick();
			}
		});
		
		Activity assistedHomeActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(assistedHomeActivity);
		assistedHomeActivity.finish();
	}
}

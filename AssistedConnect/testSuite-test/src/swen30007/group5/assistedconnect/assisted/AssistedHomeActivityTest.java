package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.id;
import swen30007.group5.assistedconnect.assisted.AlarmSentActivity;
import swen30007.group5.assistedconnect.assisted.AssistedHomeActivity;
import swen30007.group5.assistedconnect.assisted.SelectRecipientsActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

public class AssistedHomeActivityTest extends ActivityInstrumentationTestCase2<AssistedHomeActivity> {
	
	private AssistedHomeActivity mActivity;
	private Button alarmButton;
	private Button sendMessageButton;
	
	public AssistedHomeActivityTest(){
		super(AssistedHomeActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		setActivityInitialTouchMode(false);

	    mActivity = getActivity();
	    alarmButton = (Button) mActivity.findViewById(R.id.button_alarm);
	    sendMessageButton = (Button) mActivity.findViewById(R.id.button_send_message);
	}
	
	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(alarmButton != null);
	    assertTrue(sendMessageButton != null);
	}
	
	public void testAlarmButton(){
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(AlarmSentActivity.class.getName(), null, false);
		
		mActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	alarmButton.performClick();
			}
		});
		
		Activity alarmSentActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(alarmSentActivity);
		alarmSentActivity.finish();
	}
	
	public void testSendMessage() {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(SelectRecipientsActivity.class.getName(), null, false);
		
		mActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		    	sendMessageButton.performClick();
			}
		});
		
		Activity selectRecipientsActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
		// next activity is opened and captured.
		assertNotNull(selectRecipientsActivity);
		selectRecipientsActivity.finish();
	}
	
}

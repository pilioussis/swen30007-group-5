package swen30007.group5.assistedconnect.carer;

import java.util.ArrayList;
import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.carer.CarerConversationActivity;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CarerHomeActivityTest extends ActivityInstrumentationTestCase2<CarerHomeActivity>  {
	
	private CarerHomeActivity carerHomeActivity;
	private TextView titleText;
	private ListView listView;
	ContactSQL db;
	private ArrayList<Contact> contacts = new ArrayList<Contact>();
	
	public CarerHomeActivityTest() {
		super(CarerHomeActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		db = new ContactSQL(getInstrumentation().getTargetContext());
		db.dropAndCreate();
		contacts.add(new Contact("hunain", "0324233324"));
		contacts.add(new Contact("mohammed", "2424434326"));
		contacts.add(new Contact("suhain", "0524231324"));
		contacts.add(new Contact("prakesh", "0224264328"));
		contacts.add(new Contact("hinesh", "0424237724"));
		contacts.add(new Contact("olakim", "0224238321"));
		
		for(int i = 0; i< contacts.size(); i++) {
			db.addContact(contacts.get(i), true);
		}
		setActivityInitialTouchMode(false);
		
		carerHomeActivity = getActivity();
	    titleText = (TextView) carerHomeActivity.findViewById(R.id.carer_home_title);
	    listView = (ListView) carerHomeActivity.findViewById(R.id.carer_contacts);
	}
	
	@Override
	protected void tearDown() throws Exception {
		db.dropAndCreate();
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertTrue(titleText != null);
	    assertTrue(listView != null);
	}
	
	public void testContactsShown() {
		carerHomeActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				assertEquals(listView.getChildCount(),contacts.size());
				for(int i = 0;i<listView.getChildCount(); i++) {
					RelativeLayout rl = (RelativeLayout) listView.getChildAt(i);
					assertNotNull(rl);
				}
			}
		});
	}
	
	public void testSelectConversation() {
			ActivityMonitor activityMonitor = getInstrumentation().addMonitor(CarerConversationActivity.class.getName(), null, false);
			carerHomeActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					RelativeLayout rl = (RelativeLayout) listView.getChildAt(contacts.size()-1);
					rl.performClick();

				}
			});
			Activity carerConversationActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
			// next activity is opened and captured.
			assertNotNull(carerConversationActivity);
			carerConversationActivity.finish();	

	}
}

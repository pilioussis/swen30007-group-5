package swen30007.group5.assistedconnect.carer;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.carer.CarerConversationActivity;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CarerConversationActivityTest extends ActivityInstrumentationTestCase2<CarerConversationActivity>  {
	
	private CarerConversationActivity carerConversationActivity;
	TextView contactNameTxt;
	EditText editText;
	Button sendButton;
	ListView listView;
	ContactSQL ContactDb;
	MessageSQL messageDb;
	List<Message> messages;
	
	final String name = "Julia G";
	final String number = "0406424005";
	Contact contact;
	private ArrayList<Contact> contacts = new ArrayList<Contact>();
	
	public CarerConversationActivityTest() {
		super(CarerConversationActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		ContactDb = new ContactSQL(getInstrumentation().getTargetContext());
		MessageSQL messageDb = new MessageSQL(getInstrumentation().getTargetContext());
		ContactDb.dropAndCreate();
		messageDb.dropAndCreate();
		contact = new Contact(name, number);
		messages = new ArrayList<Message>();

		messages.add(new Message("11",contact.getNumber(),1));
		messages.add(new Message("22",contact.getNumber(),0));
		messages.add(new Message("33",contact.getNumber(),1));
		messages.add(new Message("44",contact.getNumber(),0));
		messages.add(new Message("55",contact.getNumber(),1));
		messages.add(new Message("66",contact.getNumber(),0));
		
		ContactDb.addContact(contact, true);
		for(int i = 0;i< messages.size();i++) {	
			messageDb.addMessage(messages.get(i));
		}

		setActivityInitialTouchMode(false);
		Intent i = new Intent();
		i.putExtra("number",contact.getNumber());
		setActivityIntent(i);
		
		carerConversationActivity = getActivity();
		contactNameTxt = (TextView) carerConversationActivity.findViewById(R.id.conversation_contact_name);
		editText =  (EditText) carerConversationActivity.findViewById(R.id.written_message);
		sendButton = (Button) carerConversationActivity.findViewById(R.id.send_message);
		listView = (ListView) carerConversationActivity.findViewById(R.id.conversation);
	}
	
	@Override
	protected void tearDown() throws Exception {
		messageDb = new MessageSQL(getInstrumentation().getTargetContext());
		ContactDb.dropAndCreate();
		messageDb.dropAndCreate();
		super.tearDown();
	}
	
	public void testPreConditions() {
	    assertNotNull(contactNameTxt);
	    assertNotNull(carerConversationActivity);
	    assertNotNull(editText);
	    assertNotNull(sendButton);
	    assertNotNull(listView); 
	}
	
	public void testContactName() {
		carerConversationActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				assertEquals(contact.getName(),contactNameTxt.getText());
			}
		});
	}
	
	public void testNumberMessages() {
		carerConversationActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				
				assertEquals(messages.size(),listView.getChildCount()); //remove four when messaging is integrated
			}
		});
	}
	
	public void testMessageOrder() {
		carerConversationActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for(int i = 0;i<listView.getCount(); i++) {
					RelativeLayout rl = (RelativeLayout) listView.getChildAt(i);
					TextView text = (TextView) rl.getChildAt(1);
					assertEquals(messages.get(i).getMessage(), text.getText());				
				}
			}
		});
	}
}

package swen30007.group5.assistedconnect.reminders;

import java.util.Calendar;

import swen30007.group5.assistedconnect.reminders.MessageReminderBroadcastReceiver;

import android.test.AndroidTestCase;

public class MessageReminderBroadcastReceiverTest extends AndroidTestCase {

    //private MockContext mContext;
	private MessageReminderBroadcastReceiver messRemBroadRec;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		messRemBroadRec = new MessageReminderBroadcastReceiver();
		//mContext = new MockContext();
	}

	@Override
	protected void tearDown() throws Exception {
		//close all resources
		super.tearDown();
	}
	
	public void testPreConditions() {
		assertTrue(messRemBroadRec != null);
	}
	
	public void testStartTime() {
		Calendar currentCalendar = Calendar.getInstance();
		for (int hours = 0; hours <= 24; hours++){
			for (int mins = 0; mins < 60; mins++){
				assertTrue(messRemBroadRec.getStartTime(hours,mins) > currentCalendar.getTimeInMillis());
			}
		}
	}
}

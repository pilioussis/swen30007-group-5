package swen30007.group5.assistedconnect.reminders;

import swen30007.group5.assistedconnect.reminders.MessageReminderService;
import android.content.Intent;
import android.test.ServiceTestCase;

public class MessageReminderServiceTest extends ServiceTestCase<MessageReminderService> {

	//private Context context;
	
	public MessageReminderServiceTest(Class<MessageReminderService> serviceClass) {
		super(serviceClass);
	}
	
	public MessageReminderServiceTest() {
		super(MessageReminderService.class);
	}

	@Override
	protected void setUp() throws Exception {
		
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testLifecycle() {
        startService(new Intent(mContext, MessageReminderService.class));
        assertTrue(getService().isCreated == 1);
        assertTrue(getService().isRunning == 1);
        
        startService(new Intent(mContext, MessageReminderService.class));
        assertTrue(getService().isCreated == 1);
        assertTrue(getService().isRunning == 2);
        
        startService(new Intent(mContext, MessageReminderService.class));
        assertTrue(getService().isCreated == 1);
        assertTrue(getService().isRunning == 3);
        
        startService(new Intent(mContext, MessageReminderService.class));
        assertTrue(getService().isCreated == 1);
        assertTrue(getService().isRunning == 4);
        
        shutdownService();
        assertTrue(getService().isCreated == 0);
        assertTrue(getService().isRunning == 0);
	}
	
	public void testReminderAlarm() {
        startService(new Intent(mContext, MessageReminderService.class));
        assertTrue(getService().alarm.isStarted);
	}
}

package swen30007.group5.assistedconnect.carer;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.adapters.CarerContactAdapter;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import swen30007.group5.assistedconnect.reminders.InactiveAlertService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ListView;
import android.widget.TextView;

public class CarerHomeActivity extends BaseActivity {
	final ContactSQL db = new ContactSQL(this);	
	List<Contact> contacts;
	ListView listView;
	private TextView carerHomeTitle;
		
	@Override
    protected void onCreate(Bundle savedInstanceState) {   	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carer_home); 
        //create new messenger object to confirm or initialise GCM registration.
        GcmMessenger gcmMessenger = new GcmMessenger(this);
        
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        carerHomeTitle = (TextView) findViewById(R.id.carer_home_title);
        
        carerHomeTitle.setTypeface(typeFace);
    }

	private void inflateContacts() {
		Intent intent = new Intent(this, CarerConversationActivity.class);
      
        listView = (ListView) findViewById(R.id.carer_contacts);
        CarerContactAdapter adapter = new CarerContactAdapter(intent,this,R.layout.carer_contact_row, contacts);
        listView.setAdapter(adapter);
	}
	
	@Override
	protected void onResume() {
		contacts = db.getAllContacts(true);
        inflateContacts(); 
        //Start inactive alert services for all contacts
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	boolean isCarer = prefs.getBoolean("user_type", false);
    	
    	if (isCarer){
            ContactSQL contactDb = new ContactSQL(this);
            Intent intent = new Intent(this, InactiveAlertService.class);
            ArrayList<String> assistedNumbers = new ArrayList<String>();
            for(Contact contact : contactDb.getAllContacts(true)){  	
            	assistedNumbers.add(contact.getNumber());
            }
            intent.putExtra("assisted_numbers", assistedNumbers);
            intent.putExtra("update_all", true);
            startService(intent);
    	}
		super.onResume();
	}
}

package swen30007.group5.assistedconnect.carer;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.adapters.ConversationAdapter;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


public class CarerConversationActivity extends BaseActivity {
	Button button;
	Message message;
	private GcmMessenger gcmMessenger;
	final MessageSQL db = new MessageSQL(this);

	final ContactSQL contactDb = new ContactSQL(this);
	final MessageSQL messageDb = new MessageSQL(this);
	List<Message> messages = new ArrayList<Message>();
	Contact contact;
	ListView listView;
	ConversationAdapter adapter;
	EditText editText;
	Handler mHandler = new Handler();
	long time;
	boolean update;
	private Button sendButton;
	private TextView contactName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gcmMessenger = new GcmMessenger(this);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();	    
		setContentView(R.layout.activity_carer_conversation);
		update = true;

        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Get views
        sendButton = (Button) findViewById(R.id.send_message);
        contactName = (TextView) findViewById(R.id.conversation_contact_name);
        
        //Set typeface to views
        sendButton.setTypeface(typeFace);
        contactName.setTypeface(typeFace);
        
	}
	@Override
	protected void onPause() {
		super.onPause();
		update = false;
	}
	
	private Contact openNormalContact() {
		String contactNumber = getIntent().getExtras().getString("number");
		return contactDb.getContact(contactNumber,true);		
	}
	
	private Contact openNotificationContact() {
		Intent i = getIntent();	
		String contactNumber = i.getStringExtra("from_number");	
		return contactDb.getContact(contactNumber,true);	
	}
	

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		update = true;
		Bundle extras = getIntent().getExtras();
		
		if (extras.containsKey("notification")) {
		    contact = openNotificationContact();

		}
		else {
			contact = openNormalContact();		
		}
		setUpView();
		
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            while (update) {
	                try {
	                    Thread.sleep(1000);
	                    mHandler.post(new Runnable() {

	                        @Override
	                        public void run() {
	                        	updateConversation();
	                        }
	                    });
	                } catch (Exception e) {
	                	Log.w("YOLO","thread error");
	                }
	            }
	        }
	       
	    }).start();
	}

	private void setUpView() {
		time = 0;
		TextView txt = (TextView) findViewById(R.id.conversation_contact_name);
		txt.setText(contact.getName());
		loadMessages();
		inflateConversation();
		button = (Button)findViewById(R.id.send_message);	
		editText = (EditText) findViewById(R.id.written_message);	
		button.setOnClickListener( new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				sendMessage();	
				hideKeyboard();			
			}
		});
	}
	
	private void loadMessages() {
		messages = messageDb.getMessagesFromContact(contact.getNumber());
	}
	
	private void inflateConversation() {
		loadMessages();
        listView = (ListView) findViewById(R.id.conversation);
        adapter = new ConversationAdapter(this,
                R.layout.conversation_left_text, messages);
        listView.setAdapter(adapter);
        listView.setSelection(adapter.getCount() - 1);    
	}
	
	private void updateConversation() {
		if(adapter.getCount() != messageDb.getMessagesFromContact(contact.getNumber()).size()) {
			
			inflateConversation();
		}
		else {
		}
	}

	public void sendMessage() {
		long newTime = System.currentTimeMillis();
		boolean send = false;
		String messageText = editText.getText().toString();
		
		if(newTime - time > 1000) {
			for(int i =0; i<messageText.length();i++) {
				if(messageText.charAt(i) != ' ')
					send = true;				
			}
			time = System.currentTimeMillis();
		}
		else { 
			send = false;
		}	
		time = System.currentTimeMillis();
		if(send)  {
			Message message = new Message(messageText,contact.getNumber(), 0);
			db.addMessage(message);

			adapter.add(message);
			adapter.notifyDataSetChanged();
			listView.setSelection(adapter.getCount() - 1);
			
			//Send Message to google
			ArrayList<String> outMessage = new ArrayList<String>();
			outMessage.add(contact.getNumber());
			gcmMessenger.sendMessage(outMessage, message.getMessage());
			
			
			
		}
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
}
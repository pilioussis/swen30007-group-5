package swen30007.group5.assistedconnect.carer;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.LocationHelper;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class AlarmReceivedActivity extends BaseActivity {
	private WebView webView;
	private static final String MAP_URL = "file:///android_asset/simplemap.html";
	private Button callBackButton;
	private TextView infoTextBox;
	private TextView addressTextBox;
	private String activeNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_received);
		
		//Get views
		webView = (WebView) findViewById(R.id.webview);
		callBackButton = (Button)findViewById(R.id.button_callback);
		infoTextBox = (TextView)findViewById(R.id.text_info);
		addressTextBox = (TextView)findViewById(R.id.text_address);
		
		//Get typeface
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set typeface on views
        callBackButton.setTypeface(typeFace);
        infoTextBox.setTypeface(typeFace);
        addressTextBox.setTypeface(typeFace);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Check device for Play Services APK.
		// checkPlayServices();
		handleNotification(getIntent());
	}

	private void handleNotification(Intent i) {
		activeNumber = i.getStringExtra("from_number");
		String location = i.getStringExtra("location");
		String name = i.getStringExtra("name");
		String address = "";
		
		if (location.length() <= 4){
			Log.w("SWAGGINS", "LOCATION BLANK");
			location = "90.0,1.0";
		}
		else{
			address = LocationHelper.getAddress(this, location);
			if (address == null) address = "";
		}
		
		setupWebView(location);
		
		int hours = Integer.parseInt(i.getStringExtra("timeStamp").substring(9, 11));
		String am_pm = "AM";
		if (hours > 12){
			hours -= 12;
			am_pm = "PM";
		}
		
		infoTextBox.setText("Alarm Received From " + name + " At " + 
							hours + ":" +
							i.getStringExtra("timeStamp").substring(11, 13) + am_pm);
		addressTextBox.setText(address);
		addressTextBox.setTextSize(18);
	}

	/** Sets up the WebView object and loads the URL of the page **/
	private void setupWebView(String location) {
		final String centerURL = "javascript:centerAt(" + location + ")";
		
		webView.getSettings().setJavaScriptEnabled(true);
		// Wait for the page to load then send the location information
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				webView.loadUrl(centerURL);
			}

		});
		webView.loadUrl(MAP_URL);
	}
	public void callBack(View view){
		try {
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:"+activeNumber));
			this.startActivity(intent);
	    } catch (ActivityNotFoundException e) {
	        Log.e("assistedconnect", "Call failed", e);
	    }
	}
}

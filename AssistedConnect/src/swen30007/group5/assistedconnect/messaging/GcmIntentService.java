/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package swen30007.group5.assistedconnect.messaging;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.assisted.AssistedConversationActivity;
import swen30007.group5.assistedconnect.carer.AlarmReceivedActivity;
import swen30007.group5.assistedconnect.carer.CarerConversationActivity;
import swen30007.group5.assistedconnect.reminders.InactiveAlertService;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int ALARM_NOTIFICATION_ID = 1;
    public static final int MESSAGE_NOTIFICATION_ID = 23;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Demo";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                //sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                //sendNotification("Deleted messages on server: " + extras.toString());
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // Post notification of received message.
            	
                //if the incoming message is an alarm, throw alarm notification
                if (Msg_IDs.ALARM_MSG_ID.compareTo(extras.getString("MSG_ID"))==0){
                	sendAlarmNotification(extras);
                }
                //if its a text message ...
                if (Msg_IDs.TEXT_MSG_ID.compareTo(extras.getString("MSG_ID"))==0){
	                //if its just a blank check in message update check-in alarm 
                	if(extras.getString("message").compareTo("")==0){
	                	updateCheckIn(extras.getString("from_number"));
	                }
                	//if its an actual message throw a message notification
                	else{
	                	sendMessageNotification(extras);
	                }
                }
                
                
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendAlarmNotification(Bundle data) {
    	String msg = data.getString("name") + " has raised an alarm.";
    	
    	mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
    	        
        Intent notificationIntent = new Intent(this, AlarmReceivedActivity.class);
        notificationIntent.putExtra("message", msg);
        notificationIntent.putExtra("name", data.getString("name"));
        notificationIntent.putExtra("from_number", data.getString("from_number"));
        notificationIntent.putExtra("location", data.getString("location"));
        notificationIntent.putExtra("timeStamp", data.getString("timestamp"));
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
        		notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
       
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.notification_alarm)
        .setContentTitle("Assisted Connect")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setContentText(msg)
        .setAutoCancel(true)
        .setDefaults(Notification.DEFAULT_SOUND)
        .setDefaults(Notification.DEFAULT_VIBRATE)
        .setLights(Color.RED, 200, 100);
        
        
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(ALARM_NOTIFICATION_ID, mBuilder.build());
    }
    
    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendMessageNotification(Bundle data) {
    	final MessageSQL messageDb = new MessageSQL(this);
    	final ContactSQL contactDb = new ContactSQL(this);
    	mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
    	String msg = data.getString("message");
    	
    	Intent notificationIntent = null;
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isCarer = prefs.getBoolean("user_type", false);
        
        if(isCarer)
        	notificationIntent	= new Intent(this, CarerConversationActivity.class);
        else
        	notificationIntent = new Intent(this, AssistedConversationActivity.class);
        	
		Message inMessage = new Message(msg,
				data.getString("from_number"),
				1);
		messageDb.addMessage(inMessage);
		Log.w("YOLO","-------------------->" + data.getString("from_number"));
		try{
			contactDb.getContact(data.getString("from_number"), true);
		}
		catch( Exception e) {
			Log.w("YOLO","+++++" + data.getString("from_number"));
			Contact newContact = new Contact(data.getString("name"),data.getString("from_number"));
			contactDb.addContact(newContact, true);
		}
        
    	notificationIntent.putExtra("notification", "1");
        notificationIntent.putExtra("message", msg);
        notificationIntent.putExtra("name", data.getString("name"));
        notificationIntent.putExtra("from_number", data.getString("from_number"));
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
        		notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
       
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.notification_text)
        .setContentTitle("Assisted Connect")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(data.getString("name") + ": "+ msg))
        .setContentText(data.getString("name") + ": "+ msg)
        .setAutoCancel(true)
        .setDefaults(Notification.DEFAULT_ALL);
        
        
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
    }
    
    //on check in- updates the carers check in alarm to a further 24 hours for relevant AP
    private void updateCheckIn(String phno){
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	boolean isCarer = prefs.getBoolean("user_type", false);
    	
    	if (isCarer){
	    	Intent intent = new Intent(this, InactiveAlertService.class);
	    	intent.putExtra("assisted_number", Contact.homogonizeNumber(phno));
	    	intent.putExtra("update_all", false);
	    	startService(intent);
    	}
    }
}

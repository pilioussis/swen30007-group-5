package swen30007.group5.assistedconnect.messaging;

import java.util.ArrayList;

public interface Messenger {
	
	/**
	* Sends a message to the phone numbers specified
	*
	* @param phoneNumbers List of destination device’s phone numbers
	* @param message Message to be sent
	*/
	public void sendMessage(ArrayList<String> phoneNumbers, String message);
	
	/**
	* Sends an alarm to all carer’s added to the application
	*/
	public void sendAlarm();
	
	/**
	* Sends a message to cancel any active alarms from the sending device to all 
	* all carer’s
	*/
	public void cancelAlarm();
	
	/**
	* Sends a message to save gcm id and phone number combination in server database
	*
	* @param phoneNumber Users own phone number
	* @param Users device gcm ID
	*/
	public void sendRegistration(String phoneNumber, String gcmID);

	
	
	
}

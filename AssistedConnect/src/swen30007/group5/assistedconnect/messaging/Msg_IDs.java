package swen30007.group5.assistedconnect.messaging;

public class Msg_IDs {
	public final static String ALARM_MSG_ID = "alarm";
	public final static String TEXT_MSG_ID = "message";
	public final static String REGISTRATION_MSG_ID = "registration";
	public final static String ALARM_CANCEL_MSG_ID = "cancel";
	public final static String USER_CHECK_MSG_ID = "check_user";

}

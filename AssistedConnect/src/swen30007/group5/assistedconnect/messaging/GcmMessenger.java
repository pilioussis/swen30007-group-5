package swen30007.group5.assistedconnect.messaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.helpers.LocationHelper;
import swen30007.group5.assistedconnect.helpers.UserDetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmMessenger implements Messenger {

	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	// SERVER APP ID
	private static String SENDER_ID = "92439767333";
	
	/**
	 * Tag used on log messages.
	 */
	private static final String TAG = "ACon";
	private String regid;
	private static GoogleCloudMessaging gcm;
	private LocationHelper location;
	private Context context;

	public GcmMessenger(Context context) {
		this.context = context;

		location = new LocationHelper();
		location.getLocation(context);
		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices(context)) {
			gcm = GoogleCloudMessaging.getInstance(context);
			regid = getRegistrationId();

			if (regid.isEmpty()) {
				registerInBackground(context);
			} else {
				Log.w("SWAGGINS", regid);
				sendRegistration(UserDetails.getPhoneNumber(context), regid);
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	/*
	 * Sends message to phoneNumbers via sendTextMessage
	 * also sends check in to all carers in list if user
	 * is an assisted person.
	 */
	@Override
	public void sendMessage(ArrayList<String> phoneNumbers, String message) {
		sendTextMessage(phoneNumbers, message);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isAssPer = !prefs.getBoolean("user_type", false);
		if(isAssPer){
			sendCheckIn();
		}
	}

	/*
	 * Sends message to phoneNumbers via gcm->server->gcm
	 */
	public void sendTextMessage(ArrayList<String> phoneNumbers, String message){
        //compile meta-data
		Time now = new Time();
        now.setToNow();
        String loc = getLocationString();
        //build message bundle
        Bundle data = new Bundle();
	    data.putString("MSG_ID", Msg_IDs.TEXT_MSG_ID);
	    data.putString("name", UserDetails.getName(context));
	    data.putString("message", message);
	    data.putString("location", loc);
	    data.putString("timestamp", now.toString());
	    //add phone numbers to recipients
        for (int i = 0; i<phoneNumbers.size(); i++){
        	data.putString("RECIPIENT." + Integer.toString(i),phoneNumbers.get(i));
        }
        //send bundle to server to be re-addressed
	    sendToServer(data);
	}
	
	private String getLocationString(){
        try{
        	String lat = ""+this.location.mostRecentLocation.getLatitude();
        	String lon = ""+this.location.mostRecentLocation.getLongitude();
        	return lat +","+lon;
        }
        //if location is null
        catch(Exception e){
        	return "";
        }
	}
	
	
	/*
	 * 
	 * Sends a blank message to all carers to confirm 'check in' and reset 
	 * non-check-in alarm 
	 * 
	 */
	public void sendCheckIn(){
		//Get all carers ph. numbers
		ContactSQL db = new ContactSQL(context);
    	List<Contact> contacts = db.getAllContacts(true);
    	ArrayList<String> allCarers = new ArrayList<String>();
    	for (Contact carer: contacts){
    		allCarers.add(Contact.homogonizeNumber(carer.getNumber()));
    	}
    	//Send blank message to all carers to confirm 'check in' occurred
    	sendTextMessage(allCarers, "");
	}
	
	
	
	/*
	 * Sends the data Bundle to the GCM python server to handle. The data bundle
	 * should include a MSG_ID string describing ' the type of message enclosed.
	 */
	void sendToServer(final Bundle data) {
		
		if (gcm != null) {
			
			final AtomicInteger msgId = new AtomicInteger();
			new AsyncTask<Void, Void, String>() {
				@Override
				protected String doInBackground(Void... params) {
					String msg = "";
					try {
					
						String id = Integer.toString(msgId.incrementAndGet());
						gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
						msg = "Sent message";
					} catch (IOException ex) {
						msg = "Error :" + ex.getMessage();
					}
					return msg;
				}

				@Override
				protected void onPostExecute(String msg) {
					// do something.
				}
			}.execute(null, null, null);
		}
	}

	/*
	 * Raise an alarm to all carers Called when ALARM button is hit from main AP
	 * view (non-Javadoc)
	 * 
	 * @see
	 * swen30007.group5.assistedconnect.messaging.Messenger#sendAlarm(android
	 * .content.Context)
	 */
	@Override
	public void sendAlarm() {
		ContactSQL db = new ContactSQL(context);
		List<Contact> contacts = db.getAllContacts(true);
		Time now = new Time();
		now.setToNow();
		String loc = getLocationString();
		if (loc==null) loc = "";
		Bundle data = new Bundle();
		data.putString("MSG_ID", Msg_IDs.ALARM_MSG_ID);
		data.putString("name", UserDetails.getName(context));
		data.putString("location", loc);
		data.putString("timestamp", now.toString());

		for (int i = 0; i < contacts.size(); i++) {
			data.putString("RECIPIENT." + Integer.toString(i), contacts.get(i)
					.getNumber());
		}
		
		sendToServer(data);
	}

	@Override
	public void cancelAlarm() {
		// TODO Auto-generated method stub

	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return context.getSharedPreferences(this.getClass().getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices(Context context) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			return false;
		}
		return true;
	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGcmPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Gets the current registration ID for application on GCM service, if there
	 * is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId() {
		final SharedPreferences prefs = getGcmPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground(final Context context) {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = null;
					regid = gcm.register(SENDER_ID);

					msg = "Device registered, registration ID=" + regid;
					Log.w("SWAGGINS", msg);
                    // Send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistration(UserDetails.getPhoneNumber(context), regid);

					// Persist the regID - no need to register again.
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Log.w("SWAGGINS", msg);
			}
		}.execute();
	}

	/**
	 * Sends the applications GCM registration ID to the backend server with the
	 * number of the accompanying phone.
	 * 
	 * @param regId
	 *            registration ID
	 * @param phoneNumber
	 *            Device�s phone number
	 */
	@Override
	public void sendRegistration(String phoneNumber, String gcmID) {
		Bundle data = new Bundle();
		data.putString("MSG_ID", Msg_IDs.REGISTRATION_MSG_ID);
		data.putString("PHNO",  phoneNumber);
		sendToServer(data);
		Log.w("SWAGGINS", "YEAJHKGAGDJAL" + phoneNumber);
	}

	public void checkUser(String phoneNumber) {
		Bundle data = new Bundle();
		data.putString("MSG_ID", Msg_IDs.USER_CHECK_MSG_ID);
		data.putString("PHNO",  phoneNumber);
		sendToServer(data);
	}

}

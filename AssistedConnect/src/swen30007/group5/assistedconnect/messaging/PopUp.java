package swen30007.group5.assistedconnect.messaging;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class PopUp {
	public PopUp(Activity act, String message){
		new AlertDialog.Builder(act)
	    .setTitle("MSG:")
	    .setMessage(message)
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        @Override
			public void onClick(DialogInterface dialog, int which) { 
	            // do nothing
	        }
	     })
	     .show();
	}
}

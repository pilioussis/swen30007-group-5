package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.VibratorWrapper;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AlarmSentActivity extends BaseActivity {

	private Button cancelAlarmButton;
	private TextView alarmSentMessage;
	protected VibratorWrapper v;
	private GcmMessenger gcmMessenger;
	private boolean cancelled;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_sent);
		
		// Show the Up button in the action bar.
		super.setupActionBar();
		
		//Get activity elements
        cancelAlarmButton = (Button)findViewById(R.id.button_cancel_alarm);
        alarmSentMessage = (TextView)findViewById(R.id.alarm_sent_message);
        
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        cancelAlarmButton.setTypeface(typeFace);
        alarmSentMessage.setTypeface(typeFace);
        
        //Get instance for messaging
        gcmMessenger = new GcmMessenger(this);
        
        //Initialize vibrator
        v = new VibratorWrapper((Vibrator) getSystemService(Context.VIBRATOR_SERVICE));
        
        cancelled = false;
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		v.startVibrator();
		
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	        	try {
		        	Thread.sleep(10000);
		        	if (!cancelled){
		        		gcmMessenger.sendAlarm();
		        	}
	        	} catch (Exception e){
	        		Log.w("YOLO","thread error");
	        	}   	
	        }
	       
	    }).start();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		v.stopVibrator();
		cancelled = true;
	}
	
	/** Called when the user clicks the Cancel Alarm button */
    public void cancelAlarm(View view) {
    	finish();
    }
    
}

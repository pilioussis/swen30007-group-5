package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.adapters.AssistedContactConvoAdapter;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

public class DisplayContactsActivity extends BaseActivity {
	final ContactSQL db = new ContactSQL(this);
	List<Contact> contacts;
	ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_display_contacts);
		contacts = new ArrayList<Contact>();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		contacts = db.getAllContacts(true);
		inflateContacts();
	}
	
	private void inflateContacts() {
		Intent intent = new Intent(this, AssistedConversationActivity.class);
		
        listView = (ListView) findViewById(R.id.assisted_contacts);
        AssistedContactConvoAdapter adapter = new AssistedContactConvoAdapter(intent,this,R.layout.assisted_contact_convo_row, contacts);
        listView.setAdapter(adapter);
	}
}

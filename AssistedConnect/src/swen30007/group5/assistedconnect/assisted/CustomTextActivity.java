package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import swen30007.group5.assistedconnect.messaging.PopUp;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class CustomTextActivity extends BaseActivity {

	private EditText mainText;
	private Button sendButton;
	private GcmMessenger gcmMessenger;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_text);
		
		//Get views
		mainText=(EditText)findViewById(R.id.written_message);
		sendButton = (Button) findViewById(R.id.button_send);
		
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        sendButton.setTypeface(typeFace);
		//Force keyboard visible
		if(mainText.requestFocus()) {
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		}
		//Initialise messaging component
		gcmMessenger = new GcmMessenger(this);
	}
	
	/** Called when used clicks the send message button */
	public void sendMessage(View view){	
		MessageSQL messageDb = new MessageSQL(this);
		String msg = mainText.getText().toString();
		if (msg == ""){
			new PopUp(this, "Please enter in a message to send and try again.");
			return;
		}
		ArrayList<String> recipients = SelectedRecipients.getRecipients();
		Message m;
    	for(int i = 0; i< recipients.size();i++) {   	
    		m = new Message((String) msg, recipients.get(i),0);
    		messageDb.addMessage(m);
    	}
		//Send message to recipients via gcm->server->gcm
		gcmMessenger.sendMessage(SelectedRecipients.getRecipients(), msg);
		//Display message sent success activity
    	Intent intent = new Intent(this, MessageSentSuccessActivity.class);
    	finish();
    	startActivity(intent);
	}
}

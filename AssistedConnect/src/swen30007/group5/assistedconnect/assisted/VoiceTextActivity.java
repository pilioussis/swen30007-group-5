package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.RectangleView;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class VoiceTextActivity extends BaseActivity {

	private Context context;
	private RelativeLayout voiceTextCont;
	private TextView speakPromptText;
	private ArrayList<RectangleView> volumeRectangles;
    private Typeface typeFace;
    protected SpeechRecognizer sr;
    private GcmMessenger gcmMessenger;
    private MessageSQL messageDb = new MessageSQL(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voice_text);
		
		context = this;
		
        //Get views
		voiceTextCont = (RelativeLayout) findViewById(R.id.voice_text_container);
		speakPromptText = (TextView) findViewById(R.id.speak_prompt_text);
        
        //Get custom type face   
        typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        speakPromptText.setTypeface(typeFace);
		
        //Initialize GCM messenger
        gcmMessenger = new GcmMessenger(this);
	}
	
    @Override
    protected void onStart() {
    	super.onStart();
    	
        //Sets up the speech recognizer to use our custom one
        sr = SpeechRecognizer.createSpeechRecognizer(this);       
        sr.setRecognitionListener(new AssistedVoiceTextListener());  
        
        startVoiceToText();
    }
	
    @Override
    protected void onStop() {
    	super.onStop();
    	
    	if (sr != null)
        {
            sr.stopListening();
            sr.cancel();
            sr.destroy();
        }
    }
    
    /** Inner class that implements a custom speech to text */
    class AssistedVoiceTextListener implements RecognitionListener          
    {
    	protected boolean speaking = false;
    	
		@Override
		public void onReadyForSpeech(Bundle params)
		{
			createBars();
		}
		@Override
		public void onBeginningOfSpeech()
		{
			speaking = true;
		}
		@Override
		public void onRmsChanged(float rmsdB)
		{
			if (speaking){
				//Converts voltage to height in pixels for the far right bar
				double barRatio = (rmsdB + 2.12) / 12.12;		
				updateBars(barRatio);	
			}
		}
		@Override
		public void onBufferReceived(byte[] buffer)
		{
		}
		@Override
		public void onEndOfSpeech()
		{
			speaking = false;
		}
		@Override
		public void onError(int error)
		{
			if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT){
				sr.stopListening();
				startVoiceToText();
			}
			else {
				switchActivityNoTransition(VoiceTextFailureActivity.class);
			}
		}
		@Override
		public void onResults(Bundle results)                   
		{
			//Send the message to selected carers
			ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);	
			
			ArrayList<String> recipients = SelectedRecipients.getRecipients();
	    	Message m;
	    	for(int i = 0; i< recipients.size();i++) {   		
	    		m = new Message(String.valueOf(data.get(0)), recipients.get(i),0);
	    		messageDb.addMessage(m);
	    	}
	    	
			gcmMessenger.sendMessage(SelectedRecipients.getRecipients(), String.valueOf(data.get(0)));
			//display message sent success activity
			switchActivityNoTransition(MessageSentSuccessActivity.class);
		}
		@Override
		public void onPartialResults(Bundle partialResults)
		{
		}
		@Override
		public void onEvent(int eventType, Bundle params)
		{
		}
	}
    
    /** Starts the process of a speech to text translation */
    public void startVoiceToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);        
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"voice.recognition.test");

        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,5); 
        sr.startListening(intent);
    }
    
    /** Creates the bars used to represent the loudness of the voice */
    private void createBars() {
		RectangleView rect;
		LayoutParams lParams;
		int numberOfBars = 10;
		int initialLeftTopMargin = 20;
		int barWidthWithMargin = (voiceTextCont.getWidth() - initialLeftTopMargin*2) / numberOfBars;
		int leftMargin = barWidthWithMargin / 8;
		int barWidth = barWidthWithMargin - leftMargin;
		int barHeight = 25;
		volumeRectangles = new ArrayList<RectangleView>();
		
		for (int i = 0; i < numberOfBars; i++){
			rect = new RectangleView(context);
			lParams = new LayoutParams(barWidth, barHeight);
			lParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			lParams.setMargins(initialLeftTopMargin + barWidthWithMargin*i, initialLeftTopMargin, 0, 0);
			rect.setLayoutParams(lParams);
			rect.setId(10);
			voiceTextCont.addView(rect);
			volumeRectangles.add(rect);
		}
    }
    
    /** 
     * Updates bars in wave like fashion 
     * @param barRatio scale between 0 to 1
     */
    private void updateBars(double barRatio) {
    	int maxBarHeight = (voiceTextCont.getHeight() / 2) - (speakPromptText.getHeight() / 2) - 10;
    	int barHeight = (int) (barRatio * maxBarHeight);
    	
    	if (barHeight < 10){
    		barHeight = 10;
    	}
    	
    	//Sets the far right bar to the latest input
		RectangleView current = volumeRectangles.get(volumeRectangles.size() - 1);
		LayoutParams oldParams = (LayoutParams) current.getLayoutParams();		
		
		LayoutParams lParams = new LayoutParams(oldParams.width, barHeight);
		lParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		lParams.setMargins(oldParams.leftMargin, oldParams.topMargin, 0, 0);
		current.setLayoutParams(lParams);
		
		LayoutParams prevParams = lParams;			
		
		//Updates the rest of the bars to the height of the bar to the right
		for (int i = volumeRectangles.size() - 2; i >= 0; i--){
			current = volumeRectangles.get(i);
			oldParams = (LayoutParams) current.getLayoutParams();
			
			lParams = new LayoutParams(oldParams.width, prevParams.height);
			lParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			lParams.setMargins(oldParams.leftMargin, oldParams.topMargin, 0, 0);
			current.setLayoutParams(lParams);
			
			prevParams = oldParams;
		}	
    }
    
    /**
     * Switches to specified activity without a transition
     * @param cls activity to switch to
     */
    protected void switchActivityNoTransition(Class<?> cls) {
        Intent intent = new Intent(context, cls);
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();

        overridePendingTransition(0, 0);
        startActivity(intent);
    }
}

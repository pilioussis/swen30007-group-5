package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.carer.CarerHomeActivity;
import swen30007.group5.assistedconnect.reminders.InactiveAlertService;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

public class SettingsActivity extends PreferenceActivity {

	private boolean isCarer;
	private SharedPreferences prefs;
	private Context context;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = getBaseContext();
		
		addPreferencesFromResource(R.xml.preferences);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		PreferenceScreen screen = getPreferenceScreen();
		Preference carerMode = findPreference("user_type");
		screen.removePreference(carerMode);
		
		isCarer = prefs.getBoolean("user_type", false);
		if(isCarer) {
			Preference reminder = findPreference("message_reminder_time");
			screen.removePreference(reminder);
		}


		Preference back_button = findPreference("back_button");
		back_button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) { 
				finish();
				return true;
			}
		});

		Preference reset_button = findPreference("reset_button");
		reset_button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) { 
				
				confirmResetDialog();
				return true;
			}
		});
	}
	
	private void resetAccount() {
		SharedPreferences.Editor editor  = prefs.edit();
		editor.putBoolean("initial_launch", true);
		editor.commit();
		
		//Drops the SQL tables 
		MessageSQL messageDb = new MessageSQL(context);
		messageDb.dropAndCreate();
		
		ContactSQL contactDb = new ContactSQL(context);
		contactDb.dropAndCreate();
		

		Intent i = getBaseContext().getPackageManager()
				.getLaunchIntentForPackage( getBaseContext().getPackageName() );
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);


		finish();
	}

	private void confirmResetDialog() {
		//if the contact was not associated with any number
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?")
        .setMessage("Resetting will remove all your contacts, messages and preferences")
        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
            	resetAccount();
                dialog.cancel();
            }
        })
        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	
	@Override 
	public void onResume() {
		super.onResume();
		isCarer = prefs.getBoolean("user_type", false);
		Preference p = findPreference( "initial_launch" );
		getPreferenceScreen().removePreference( p );
	}

	@Override
	public void onStop(){
		super.onStop();
		boolean isCarerAfter = prefs.getBoolean("user_type", false);

		//If the setting has been changed, switch UI's
		if (isCarer != isCarerAfter){    		
			Intent intent;
			if (isCarerAfter){
				intent = new Intent(this, CarerHomeActivity.class);
			}
			else {
				intent = new Intent(this, AssistedHomeActivity.class);
				stopService(new Intent(this, InactiveAlertService.class));
			}
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
		}
	}


}
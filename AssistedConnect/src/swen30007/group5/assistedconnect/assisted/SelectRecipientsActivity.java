package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.adapters.RecipientsAdapter;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SelectRecipientsActivity extends BaseActivity {

	private Context context;
	private Button doneButton;
	private GridView gridview;
	private Toast currentToast;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_recipients);
		
		//Get context
		context = this;
		
		// Show the Up button in the action bar.
		super.setupActionBar();
		
		//Get activity elements
        doneButton = (Button)findViewById(R.id.button_done_recipients);
        gridview = (GridView) findViewById(R.id.gridview_contacts);
	
		//Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        doneButton.setTypeface(typeFace);
        
	}
	
	@Override
	protected void onResume() {
		super.onResume();
        
        //Adds in the contacts into the grid
        gridview.setAdapter(new RecipientsAdapter(this));
		
        //Assign action for clicking on contact in grid
        gridview.setOnItemClickListener(new OnItemClickListener() {
            @Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            	View overlay = v.findViewById(R.id.overlay);
            	TextView number = (TextView) v.findViewById(R.id.number);
            	
            	//Checks to see if grid item is a contact or the add contact option
            	if (number.getText() == "-1"){
            		Intent intent = new Intent(context, ChangeContactsActivity.class);
    	        	startActivity(intent);
            	}
            	else {
            		//Toggles if contact is recipient or not
	            	if (overlay.getVisibility() == View.GONE){
	            		overlay.setVisibility(View.VISIBLE);
	            		SelectedRecipients.addRecipient((String) number.getText());
	            	}
	            	else {
	            		overlay.setVisibility(View.GONE);
	            		SelectedRecipients.removeRecipient((String) number.getText());
	            	}
            	}
            }
        });
	}
	
	/** Called when the user clicks the done button */
    public void doneRecipients(View view) {
    	//Checks to see if a recipient has been selected and displays
    	//a toast if no recipient has been selected
    	if (SelectedRecipients.getRecipients().isEmpty()){
    		createToastMessage();
    	}
    	else {
	    	Intent intent = new Intent(this, MessageModeActivity.class);
	        startActivity(intent);
    	}
    }
    
    /** Creates a toast message that requests the user to select contacts */
    private void createToastMessage() {
    	if (currentToast != null){
    		currentToast.cancel();
    	}
		currentToast = Toast.makeText(getApplicationContext(), R.string.toast_need_recipients, Toast.LENGTH_SHORT);
		currentToast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) currentToast.getView();
		TextView newToastTV = (TextView) toastLayout.getChildAt(0);
		newToastTV.setGravity(Gravity.CENTER);
		newToastTV.setTextSize(30);
		currentToast.show();
    }
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MessageModeActivity extends BaseActivity{

    private Button textButton;
    private Button voiceButton;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg_mode);
        	
        textButton=(Button)findViewById(R.id.button_text);
        voiceButton=(Button)findViewById(R.id.button_voice);
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        textButton.setTypeface(typeFace);
        voiceButton.setTypeface(typeFace);

    }

    /** Called when the user clicks the voice to text button */
    public void voiceMessage(View view) {
        Intent intent = new Intent(this, VoiceTextCountdownActivity.class);
        startActivity(intent);
    }

    /** Called when the user clicks the Text button */
    public void textMessage(View view){
        Intent intent = new Intent(this, TextMessageActivity.class);
        startActivity(intent);
    }


}

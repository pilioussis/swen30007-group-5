package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TextMessageActivity extends BaseActivity{

    private Button default1Button;
    private Button default2Button;
    private Button default3Button;
    private Button default4Button;
    private Button customTextButton;
    private GcmMessenger gcmMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_text);
        initButtons();
        gcmMessenger = new GcmMessenger(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        //refresh buttons incase they were just changed in settings
        initButtons();
    }

    /** Called when the user clicks a pre-set message button */
    public void sendText(View view) {
    	ArrayList<String> recipients = SelectedRecipients.getRecipients();
    	MessageSQL messageDb = new MessageSQL(this);
    	TextView textView = (TextView) view;
    	//send preset message to selected carers
    	Message m;
    	for(int i = 0; i< recipients.size();i++) {   	
    		m = new Message((String) textView.getText(), recipients.get(i),0);
    		messageDb.addMessage(m);
    	}
    	gcmMessenger.sendMessage(SelectedRecipients.getRecipients(), (String) textView.getText());
    	//display message sent success activity
    	Intent intent = new Intent(this, MessageSentSuccessActivity.class);
    	finish();
    	startActivity(intent);
    }

    /** Called when the user clicks the Send Message button */
    public void sendCustomText(View view){
        Intent intent = new Intent(this, CustomTextActivity.class);
        startActivity(intent);
    }

    /** Sets up the buttons to display their respective custom text*/
    private void initButtons(){
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        //get buttons
        default1Button=(Button)findViewById(R.id.button_def_1);
        default2Button=(Button)findViewById(R.id.button_def_2);
        default3Button=(Button)findViewById(R.id.button_def_3);
        default4Button=(Button)findViewById(R.id.button_def_4);
        customTextButton=(Button)findViewById(R.id.button_custom);
        //set font
        default1Button.setTypeface(typeFace);
        default2Button.setTypeface(typeFace);
        default3Button.setTypeface(typeFace);
        default4Button.setTypeface(typeFace);
        customTextButton.setTypeface(typeFace);
        //get custom messages from preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String defaultString1 = prefs.getString(getString(R.string.pref_def_text_1_key), getString(R.string.pref_def_text_1_val));
        String defaultString2 = prefs.getString(getString(R.string.pref_def_text_2_key), getString(R.string.pref_def_text_2_val));
        String defaultString3 = prefs.getString(getString(R.string.pref_def_text_3_key), getString(R.string.pref_def_text_3_val));
        String defaultString4 = prefs.getString(getString(R.string.pref_def_text_4_key), getString(R.string.pref_def_text_4_val));
        //set button labels
        default1Button.setText(defaultString1);
        default2Button.setText(defaultString2);
        default3Button.setText(defaultString3);
        default4Button.setText(defaultString4);
    }
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import swen30007.group5.assistedconnect.reminders.MessageReminderService;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AssistedHomeActivity extends BaseActivity {
	
	private Button alarmButton;
	private Button sendMessageButton;
	private Button seeConversations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {   	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); 
       
        //Get views
        alarmButton=(Button)findViewById(R.id.button_alarm);
        sendMessageButton=(Button)findViewById(R.id.button_send_message);
        seeConversations=(Button)findViewById(R.id.see_conversations);
        
        //Get typeface
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set typeface on views
        alarmButton.setTypeface(typeFace);
        sendMessageButton.setTypeface(typeFace);
        
        //Start reminder service
        startService(new Intent(this, MessageReminderService.class));
    }
          
    /** Called when the user clicks the Alarm button */
    public void sendAlarm(View view) {
    	Intent intent = new Intent(this, AlarmSentActivity.class);
        startActivity(intent);
    }

    /** Called when the user clicks the Send Message button */
    public void sendMessage(View view){
        Intent intent = new Intent(this, SelectRecipientsActivity.class);
        startActivity(intent);
    } 
}

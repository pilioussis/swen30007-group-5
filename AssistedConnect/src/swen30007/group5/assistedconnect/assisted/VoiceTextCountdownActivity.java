package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

public class VoiceTextCountdownActivity extends BaseActivity {

	private Context context;
    private TextView countdownText;
    private Typeface typeFace;
    private CountDownTimer countdownTimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voice_text_countdown);
		
		context = this;
		
        countdownText = (TextView) findViewById(R.id.countdown_text);
        
        //Get custom type face   
        typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        countdownText.setTypeface(typeFace);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		startCountdown();
	}
	
	@Override
	protected void onStop(){
		super.onStop();	
		countdownTimer.cancel();
	}
	
	/** Starts the countdown and switches to next activity when finished */
    private void startCountdown() {
    	countdownTimer = new CountDownTimer(6000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
        		countdownText.setText("" + millisUntilFinished / 1000);
			}		
			@Override
			public void onFinish() {
		        Intent intent = new Intent(context, VoiceTextActivity.class);
		        overridePendingTransition(0, 0);
		        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		        finish();

		        overridePendingTransition(0, 0);
		        startActivity(intent);
			}
        };
        
        countdownTimer.start();
    }
}

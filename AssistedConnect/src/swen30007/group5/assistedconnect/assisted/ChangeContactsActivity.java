package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.adapters.AssistedContactAdapter;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

public class ChangeContactsActivity extends BaseActivity {
	
	final int PICK_CONTACT = 1234;
	TextView contactInfo;
	TableLayout contactTable;
	final ContactSQL db = new ContactSQL(this);
	ListView listView;
	AssistedContactAdapter adapter;
	private Button newButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);
		setTable();
		
		//Get Views
		newButton = (Button) findViewById(R.id.see_conversations);
		
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        newButton.setTypeface(typeFace);
	}
	
	//called when "pick contact" pressed in layout
	public void pickContact(View view) {
		//changes intent to android's inbuilt contact selector
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, PICK_CONTACT);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		//when returning from pick contact
		if(requestCode == PICK_CONTACT) {		
			//if the user presses back or cancels prematurely, stop
			if(resultCode != RESULT_CANCELED) {
				addContact(resultCode, data);				    					
			}
			else {
				//the user pressed back while in the android contact picker
			}			
		}	
	}
	
	private void addContact(int resultCode, Intent data)  {
		final Contact contact = new Contact(); // must be final for async dialog
		String id,hasPhone;
		int id_index, name_index, hasphone_index;
	
		//look up the result set returned by androids contact picker
		Uri contactUri = data.getData();
		Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
		
		//move to the first element of result, skips if null
		if (cursor.moveToFirst()) {
	
		    id_index = cursor.getColumnIndex(BaseColumns._ID);
		    id = cursor.getString(id_index);

		    name_index = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
		    //store name in contact class
		    contact.setName(cursor.getString(name_index));//-------------------------------------
		    				    
		    hasphone_index = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
		    hasPhone = cursor.getString(hasphone_index);
		   			    
		    getNumberFromContact(id, contact, hasPhone);			    
		}				
	}
	
	private void insertContactAndCheck(Contact contact) {
		boolean exists = db.checkIfContactExists(contact.getNumber(),true);
    	/*
    	 * boolean registered = false;
    	 * if (exists){
    	 * 	gcmMessenger.checkUser(contact.getNumber();
    	 * 	TODO: 
    	 *  wait for incoming message of type USER_CHECK_MSG_ID
    	 *  result  = msg.extras.getString("result") 
    	 * registered = (result== "true");
    	 * }
    	 * 
    	 * if(registered){
    	 */
    	if(!exists) {
    		db.addContact(contact,true);
    		adapter.add(contact);
    		adapter.notifyDataSetChanged();
    	}
    	else {
    		numberAlreadyExistsDialog();
    	}
	}
	
	private void getNumberFromContact(String id, final Contact contact, String hasPhone) {
		ArrayList<String> numbers;
			
		//if a number is associated with that contact
		if(hasPhone.equals("1")) {		
			numbers = androidNumberTableLookup(id);
			if(numbers.size() == 1) {
				//set the primary number (the number of the phone with the app) keep other numbers in case??
				contact.setNumber(numbers.get(0));
				insertContactAndCheck(contact);	    	
			}
			else {
				manyNumberDialog(numbers, contact);
			}					
		}
		else {
			this.noNumberDialog();				
		}
	}
	
	private ArrayList<String> androidNumberTableLookup(String id) {
		ArrayList<String> numbers = new ArrayList<String>();
		ContentResolver cr = getContentResolver();
		//get all the number(s) associated with the contact
		Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
			        			null,
			        			ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { Uri.encode(id) + "" },
			         			null);
		//add all the numbers to the contact class
		while (pCur.moveToNext()) {
			  String phoneNo =   pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			  numbers.add(phoneNo);
		}
		return numbers;
	}
	
	private void manyNumberDialog(ArrayList<String> numbers, final Contact contact) {
		//convert String array to final for use in other thread
		String[] numberArray = new String[numbers.size()];
	    numberArray = numbers.toArray(numberArray);						    
	    final String[] finalNumberArray = numberArray;
	    
		//run dialog for asking user to select primary 
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select the number from this contact");					
		builder.setItems(numberArray, new DialogInterface.OnClickListener() {
		    @Override
			public void onClick(DialogInterface dialog, int item) {
		    	//add number then add to the database
		    	contact.setNumber(finalNumberArray[item]);
		    	insertContactAndCheck(contact);            
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();				
	}
	
	private void noNumberDialog() {
		//if the contact was not associated with any number
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sorry")
        .setMessage("The contact you chose had no phone numbers.")
        .setCancelable(false)
        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	private void numberAlreadyExistsDialog() {
		//if the contact was not associated with any number
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sorry")
        .setMessage("This number is already in your contact list")
        .setCancelable(false)
        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	
/**************************************************************************************************************/
	
	private void setTable() {
		
		List<Contact> contacts = db.getAllContacts(true);
		
		listView = (ListView) findViewById(R.id.carer_contacts);
        adapter = new AssistedContactAdapter(this,R.layout.assisted_contact_row, contacts);
        listView.setAdapter(adapter);

    }
}


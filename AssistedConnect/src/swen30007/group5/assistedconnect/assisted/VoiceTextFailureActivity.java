package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class VoiceTextFailureActivity extends BaseActivity {

	private TextView errorPromptText;
	private Button buttonVoiceTextRetry;
	private Typeface typeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voice_text_failure);
		
        //Get views
		errorPromptText = (TextView) findViewById(R.id.error_prompt_text);
		buttonVoiceTextRetry = (Button) findViewById(R.id.button_voice_text_retry);
        
        //Get custom type face   
        typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        errorPromptText.setTypeface(typeFace);
        buttonVoiceTextRetry.setTypeface(typeFace);
	}
	
	public void restartVoiceText(View v){
		Intent intent = new Intent(this, VoiceTextCountdownActivity.class);
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();

        overridePendingTransition(0, 0);
        startActivity(intent);
	}
}

package swen30007.group5.assistedconnect.assisted;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MessageSentSuccessActivity extends BaseActivity {

	private TextView successPromptText;
	private Button buttonVoiceTextFinish;
	private Typeface typeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_sent_success);
		
        //Get views
		successPromptText = (TextView) findViewById(R.id.success_prompt_text);
		buttonVoiceTextFinish = (Button) findViewById(R.id.button_voice_text_finish);
        
        //Get custom type face   
        typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        successPromptText.setTypeface(typeFace);
        buttonVoiceTextFinish.setTypeface(typeFace);
	}
	
	/** Called when user presses the finish button */
	public void finishMessage(View v){
		Intent intent = new Intent(this, AssistedHomeActivity.class);
        finish();
        startActivity(intent);
	}

}

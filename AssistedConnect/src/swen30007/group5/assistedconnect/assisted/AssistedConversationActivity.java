package swen30007.group5.assistedconnect.assisted;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.adapters.AssistedConversationAdapter;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


public class AssistedConversationActivity extends Activity {
	private GcmMessenger gcmMessenger;
	final MessageSQL db = new MessageSQL(this);

	final ContactSQL contactDb = new ContactSQL(this);
	final MessageSQL messageDb = new MessageSQL(this);
	List<Message> messages = new ArrayList<Message>();
	Contact contact;
	ListView listView;
	Button sendMessage;
	AssistedConversationAdapter adapter;
	boolean update;
	Handler mHandler = new Handler();
	private TextView contactName;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gcmMessenger = new GcmMessenger(this);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();	    
		setContentView(R.layout.activity_assisted_conversation);
		sendMessage = (Button) findViewById(R.id.send_message_from_convo);
		final Intent i = new Intent(this, MessageModeActivity.class);
		sendMessage.setOnClickListener( new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				SelectedRecipients.clearRecipients();
				SelectedRecipients.addRecipient(contact.getNumber());
				startActivity(i);
			}
		});
		
		contactName = (TextView) findViewById(R.id.conversation_contact_name);
		
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set Elements to the custom typeface
        contactName.setTypeface(typeFace);
        sendMessage.setTypeface(typeFace);
	}
	@Override
	protected void onPause() {
		super.onPause();
		update = false;
	}
	private Contact openNormalContact() {
		String contactNumber = getIntent().getExtras().getString("number");
		return contactDb.getContact(contactNumber,true);		
	}
	
	private Contact openNotificationContact() {
		Intent i = getIntent();
		String contactNumber = i.getStringExtra("from_number");	
		return contactDb.getContact(contactNumber,true);	
	}
	

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Bundle extras = getIntent().getExtras();		
		if (extras.containsKey("notification")) {
		    contact = openNotificationContact();
		}
		else {
			contact = openNormalContact();	
		}		
		setUpView();
		update = true;
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            while (update) {
	                try {
	                    Thread.sleep(1000);
	                    mHandler.post(new Runnable() {

	                        @Override
	                        public void run() {
	                        	updateConversation();
	
	                            
	                        }
	                    });
	                } catch (Exception e) {
	                	Log.w("YOLO","thread error");
	                }
	            }
	        }
	       
	    }).start();
	}

	private void setUpView() {
		TextView txt = (TextView) findViewById(R.id.conversation_contact_name);
		txt.setText(contact.getName());
		inflateConversation();
		
	}
	
	private void loadMessages() {
		messages = messageDb.getMessagesFromContact(contact.getNumber());
	}
	
	private void inflateConversation() {
		
		messages = messageDb.getMessagesFromContact(contact.getNumber());
        listView = (ListView) findViewById(R.id.conversation);
        adapter = new AssistedConversationAdapter(this,
                R.layout.assisted_conversation_left_text, messages);
        listView.setAdapter(adapter);
        listView.setSelection(adapter.getCount() - 1);    
	}
	private void updateConversation() {
		if(adapter.getCount() != messageDb.getMessagesFromContact(contact.getNumber()).size()) {		
			inflateConversation();
		}
	}
}
package swen30007.group5.assistedconnect.SQL;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MessageSQL  extends SQLiteOpenHelper {

                 
            // Database Version
            private static final int DATABASE_VERSION = 1;
         
            // Database Name
            private static final String DATABASE_NAME = "messagesManager";
         
            // Table Name
            private static final String TABLE_MESSAGES = "messages";
         
            // Column names
            private static final String KEY_MESSAGE = "message";
            private static final String KEY_DATE = "date";
            private static final String KEY_CONTACT = "contact";
            private static final String KEY_INCOMING = "incoming";
            
            private static final String KEY_ID = "id";
         
            public MessageSQL(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }
         
            // Creating Tables
            @Override
            public void onCreate(SQLiteDatabase db) {
                String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_MESSAGES + "("
                        + KEY_MESSAGE + " TEXT,"
                        + KEY_DATE + " DATE,"
                        + KEY_CONTACT + " TEXT,"
                        + KEY_INCOMING + "INTEGER,"
                        + KEY_ID + " INTEGER PRIMARY KEY" + ")";
                db.execSQL(CREATE_CONTACTS_TABLE);
            }
         
            // When changing version
            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                // Drop older table if existed
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
         
                // Create tables again
                onCreate(db);
            }
            
            //Reset table
            public void dropAndCreate() {
                SQLiteDatabase db = this.getWritableDatabase();

                db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
             
                onCreate(db);

	        db.close(); // Closing database connection
	    }
	    
	    //insert message to database
	    public void addMessage(Message message) {
	    	String safeString = message.getMessage();
	    	safeString = safeString.replace("'","''");
//	    	safeString = safeString.replace("\"","\\\"");

	        SQLiteDatabase db = this.getWritableDatabase();
	        String query = "INSERT INTO "+TABLE_MESSAGES+ " VALUES ( " +
	        		"'" + safeString + "'," +
	        		" datetime()," +
	        		"'" + message.getContact() + "'," +
	        		message.getIncoming() + "," +
	        		"null)";
	        db.execSQL(query);
	     
	
	        db.close(); // Closing database connection
	    }
	    
	    //get a message with particular id
	    public Message getMessage(String id) {
	        SQLiteDatabase db = this.getReadableDatabase();
	        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_MESSAGES + " WHERE " + KEY_ID + " = " + id, null);
	        if (cursor != null)
	            cursor.moveToFirst();
	     
	        Message message = new Message(cursor.getString(0), cursor.getString(1),cursor.getString(2),cursor.getInt(3),cursor.getInt(4));
	        db.close();
	        return message;
	    }
	    
	    //remove a message with particular id
	    public void deleteMessage(String id) {
	            SQLiteDatabase db = this.getWritableDatabase();
	            db.delete(TABLE_MESSAGES, KEY_ID + " = ?",
	                    new String[] { id });
	            db.close();
	    	    	
	    }
	    
	    //get all messages from a specific contact
	    public List<Message> getMessagesFromContact(String contact) {
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	List<Message> messageList = new ArrayList<Message>();
	    	String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + KEY_CONTACT + " = '" + contact + "' ORDER BY " + KEY_ID;
	    	Cursor cursor = db.rawQuery(query,null);
	    	
	    	if (cursor.moveToFirst()) {
	            do {

                        Message message = new Message(cursor.getString(0), cursor.getString(1),cursor.getString(2),cursor.getInt(3),cursor.getInt(4));

                        // Adding contact to list
                        messageList.add(message);

                    } while (cursor.moveToNext());      
                }
                return messageList;
            }
            
            //returns the total number of messages in table
            public int getTotalMessageCount() {
                SQLiteDatabase db = this.getReadableDatabase();
                String query = "SELECT * FROM " + TABLE_MESSAGES;
                Cursor cursor = db.rawQuery(query,null);
                return cursor.getCount();
            }
            
            //gets the latest incoming message from a particular contact
            public Message getNewestMessage(String contact) {           
                SQLiteDatabase db = this.getReadableDatabase();                         
                String idQuery = "SELECT MAX(" + KEY_ID + ") FROM " + TABLE_MESSAGES;
                Cursor cursor = db.rawQuery(idQuery,null);
                if (cursor != null)
                    cursor.moveToFirst();
                int id = cursor.getInt(0);
                
                Message message = this.getMessage(id+"");
                db.close();
                return message;

                
            }
            
}

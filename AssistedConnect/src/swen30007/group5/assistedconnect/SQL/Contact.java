package swen30007.group5.assistedconnect.SQL;

import java.io.Serializable;



public class Contact implements Serializable{
	private static final long serialVersionUID = 1902693822694193960L;
	String name;
	String number;
	
	public Contact(String name, String number) {
		this.name=name;
		this.number = homogonizeNumber(number);
	}
	
	public Contact() {
		this.name = "undefined";
		this.number = "noNumber";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}
	
	public String getNormalNumber() {
		return normalizeNumber(this.number);
	}

	public void setNumber(String number) {
		this.number = homogonizeNumber(number);
	}
	
	//Changes 040 ten digit style phone numbers into +61
	//TODO error handling on home phone numbers etc.
	public static String homogonizeNumber(String number){
		String internationalNumber = number.trim().replace(" ", ""); 
		if (internationalNumber.length() == 10){
			internationalNumber = "+61" + internationalNumber.substring(1);
		}
		if(internationalNumber.startsWith("+")){		
			return internationalNumber;
		}
		//else return ERROR.
		return internationalNumber;
	}
	
	public String normalizeNumber(String number) {
		String normalNumber = "0" + number.substring(3);
		return normalNumber;
	}
	
}

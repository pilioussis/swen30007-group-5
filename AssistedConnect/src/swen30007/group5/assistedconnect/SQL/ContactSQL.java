package swen30007.group5.assistedconnect.SQL;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ContactSQL extends SQLiteOpenHelper {
         
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "contactsManager";
 
    // Table Name
    private static final String TABLE_CARER_CONTACTS = "carercontacts";
    private static final String TABLE_ASSISTED_CONTACTS = "assistedcontacts";
 
    // Column names
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";
 
    public ContactSQL(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CARER_TABLE = "CREATE TABLE " + TABLE_CARER_CONTACTS + "("
                + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT PRIMARY KEY" + ")";
        db.execSQL(CREATE_CARER_TABLE);
        
        String CREATE_ASSISTED_TABLE = "CREATE TABLE " + TABLE_ASSISTED_CONTACTS + "("
                + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT PRIMARY KEY" + ")";
        db.execSQL(CREATE_ASSISTED_TABLE);
    }
 
    // Reset Tables
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARER_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSISTED_CONTACTS);
        
 
        // Create tables again
        onCreate(db);
    }
    
    //Reset table
    public void dropAndCreate() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSISTED_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARER_CONTACTS);
     
        onCreate(db);

        db.close(); // Closing database connection
    }
    
    public void addContact(Contact contact, boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        
        SQLiteDatabase db = this.getWritableDatabase();
     
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PH_NO, contact.getNumber());
     
        // Inserting Row
        db.insert(currentTable, null, values);
        db.close(); // Closing database connection
    }
    
    public Contact getContact(String phone, boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
     
        Cursor cursor = db.query(currentTable, new String[] {
                KEY_NAME, KEY_PH_NO }, KEY_PH_NO + "=?",
                new String[] { phone }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
     
        Contact contact = new Contact(cursor.getString(0), cursor.getString(1));
        db.close();
        return contact;
    }
    
    public void deleteContact(String phone, boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(currentTable, KEY_PH_NO + " = ?",
                new String[] { phone });
        db.close();
    }
    
    public List<Contact> getAllContacts(boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        List<Contact> contactList = new ArrayList<Contact>();

        String selectQuery = "SELECT  * FROM " + currentTable;
     
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
     
        // loop through all rows adding to list

        if (cursor.moveToFirst()) {
            do {

                Contact contact = new Contact();
                
                contact.setName(cursor.getString(0));
                contact.setNumber(cursor.getString(1));
                // Adding contact to list
                contactList.add(contact);

            } while (cursor.moveToNext());

        }
     
        // return contact list
        return contactList;
    }
    
    // Contact Count
    public int getContactsCount(boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        String countQuery = "SELECT  * FROM " + currentTable;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

 
        return cursor.getCount();
    }
    
    //returns all contacts in a list to be displayed
    public String getAllContactsString(boolean isAssisted) {
        String allContactsDisplay = "";
                
                for(Contact swag:this.getAllContacts(isAssisted)) {                             
                        allContactsDisplay = allContactsDisplay + (swag.name + ": " + swag.number + "\n");
                }
                return allContactsDisplay;
    }
    
    //checks if a contact already exists with number
    public boolean checkIfContactExists(String number, boolean isAssisted) {
        String currentTable;
        if(isAssisted) 
                currentTable = TABLE_ASSISTED_CONTACTS;
        else
                currentTable = TABLE_CARER_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT 1 FROM "  + currentTable + " WHERE " + KEY_PH_NO + "= '" + number + "'",null);
           boolean exists = (cursor.getCount() > 0);
           cursor.close();
           return exists;

    }
    
    
    
    
    
    
}
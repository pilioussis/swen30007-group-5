package swen30007.group5.assistedconnect.SQL;

import java.util.Date;

public class Message {
        String message; //the actual text component of the message
        String date; //the time received or sent
        String contact; // the contact sent to or received by
        int incoming;  //if the message was incoming or outgoing. 1=incoming, 0=outgoing
        int id; // the id of the message in the SQL table

        
        public String getDate() {
                return date;
        }

        public Message(String message, String from, int incoming) {
                this.message = message;
                this.contact = from;
                this.date = new Date().toString();
                this.incoming = incoming;
        }
        
        public Message(String message, String date, String from,int incoming, int id) {
                this.message = message;
                this.contact = from;
                this.date = date;
                this.id = id;
                this.incoming = incoming;

        }
        
        public int getIncoming() {
                return incoming;
        }

        public void setIncoming(int incoming) {
                this.incoming = incoming;
        }

        @Override
        public String toString() {
                return "Contact = " + this.contact + "   message = " + this.message + "    date = " + this.date + "    incoming = " + this.incoming + "    (id=" +this.id + ")";
                
        }
        

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }
        
        public void setDate(String date) {
                this.date = date;
        }

        public String getMessage() {
                return message;
        }
        public void setMessage(String message) {
                this.message = message;
        }
        public String getContact() {
                return contact;
        }
        public void setContact(String from) {
                this.contact = from;
        }
        
}
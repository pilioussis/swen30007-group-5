package swen30007.group5.assistedconnect.helpers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationHelper implements LocationListener{
	public Location mostRecentLocation;
 	public void getLocation(Context c) {      
		LocationManager locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = locationManager.getBestProvider(criteria, true);

		// In order to make sure the device is getting location, request
		// updates. locationManager.requestLocationUpdates(provider, 1, 0,
		// this);
		 mostRecentLocation = locationManager.getLastKnownLocation(provider);
		// locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, null);
		// mostRecentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	/** Sets the mostRecentLocation object to the current location of the device **/
	@Override
	public void onLocationChanged(Location location) {
		mostRecentLocation = location;
	}

	/**
	 * The following methods are only necessary because WebMapActivity
	 * implements LocationListener
	 **/
	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
	//takes in lat/long string (comma separated) returns address
	static public  String getAddress(Context c, String location){
		if (location == "") return "";
		String[] latlon = location.split("\\s*,\\s*");

		Geocoder geocoder;
		List<Address> addresses = null;
		geocoder = new Geocoder(c, Locale.getDefault());
		try {
			addresses = geocoder.getFromLocation(Double.parseDouble(latlon[0]),
					Double.parseDouble(latlon[1]), 1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String address = addresses.get(0).getAddressLine(0);
		String city = addresses.get(0).getAddressLine(1);
		String country = addresses.get(0).getAddressLine(2);
		
		return address + ", " + city + ", " + country;
	}
}

package swen30007.group5.assistedconnect.helpers;

import android.os.Vibrator;


public class VibratorWrapper {
	private Vibrator v;
	private boolean isVibrating;
	private final long[] pattern = {0,1000};
	
    public VibratorWrapper (Vibrator v) {
    	this.v = v;
    	this.isVibrating = false;
    }
    
	public boolean isVibrating() {
		return isVibrating;
	}

	public void setIsVibrating(boolean isVibrating) {
		this.isVibrating = isVibrating;
	}
    
    /** Starts the vibrator */
    public void startVibrator() {
        // Vibrate continuously
        v.vibrate(pattern, 0);
        
        setIsVibrating(true);
    }
    
    /** Stops the vibrator */
    public void stopVibrator() {
    	if (v != null){
    		v.cancel();
    	}
    	setIsVibrating(false);
    }
    
}

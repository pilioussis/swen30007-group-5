package swen30007.group5.assistedconnect.helpers;

import java.util.ArrayList;

final public class SelectedRecipients {
	private static ArrayList<String> selectedRecipients = new ArrayList<String>();
	
	public static ArrayList<String> getRecipients(){
		return selectedRecipients;
	}
	
	public static void addRecipient(String phoneNumber){
		selectedRecipients.add(phoneNumber);
	}
	
	public static void removeRecipient(String phoneNumber){
		selectedRecipients.remove(phoneNumber);
	}
	
	public static void clearRecipients(){
		selectedRecipients.clear();
	}
	
	public static boolean hasRecipient(String phoneNumber){
		return selectedRecipients.contains(phoneNumber);
	}
}

package swen30007.group5.assistedconnect.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;


public class UserDetails {
	//REturns users name for use in notification on other users phones 
	//e.g. alarm from Bob Bobberson.
	public static String getName(Context c){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        String name = prefs.getString("username", "default username");
        return name;
	}
	
	
	//Returns users phone number in the format +YYxxxxxxxxx 
	//	where YY is the country code 
	public static String getPhoneNumber(Context c){
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);   
		String yourNumber = "" + mTelephonyMgr.getLine1Number();	

		if(yourNumber == null || yourNumber.length() < 7) {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
	        yourNumber = swen30007.group5.assistedconnect.SQL.Contact.homogonizeNumber((prefs.getString("number", "69")));
	        
		}
		return yourNumber;
	}
	
	public static boolean attempAutoNumberSet(Context c){
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);   
		String yourNumber = "" + mTelephonyMgr.getLine1Number();	
		//if you couldnt get ph no from sim
		if(yourNumber == null || yourNumber.length() < 7) {
			return false;
		}
		//if you could get number from sim
		else{
			//store it in prefs and return true
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(c);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("number", yourNumber);
			editor.commit();
			return true;
		}
	}
}

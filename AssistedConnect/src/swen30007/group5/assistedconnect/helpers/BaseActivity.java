package swen30007.group5.assistedconnect.helpers;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.assisted.ChangeContactsActivity;
import swen30007.group5.assistedconnect.assisted.DisplayContactsActivity;
import swen30007.group5.assistedconnect.assisted.SettingsActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

public abstract class BaseActivity extends Activity {

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isCarer = prefs.getBoolean("user_type", false);
        
        if (isCarer){
        	getMenuInflater().inflate(R.menu.carer_menu, menu);
        }
        else {
        	getMenuInflater().inflate(R.menu.assisted_menu, menu);
        }
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isCarer = prefs.getBoolean("user_type", false);
    	
        if (isCarer){
        	switch (item.getItemId()) { 
		        case R.id.action_settings:
		        	startActivity(new Intent(this, SettingsActivity.class));
		            break;
		        case android.R.id.home:
					NavUtils.navigateUpFromSameTask(this);
					break;
        	}
        }
        else {
	        switch (item.getItemId()) { 
		        case R.id.action_messages:
		        	startActivity(new Intent(this, DisplayContactsActivity.class));
		        	break;
		        case R.id.action_settings:
		        	startActivity(new Intent(this, SettingsActivity.class));
		            break;
		        case R.id.change_carers:
		        	startActivity(new Intent(this, ChangeContactsActivity.class));
		            break;
		        case android.R.id.home:
					NavUtils.navigateUpFromSameTask(this);
					break;
	        }
        }
        return true;
    }
	
}

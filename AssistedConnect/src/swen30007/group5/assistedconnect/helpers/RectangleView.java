package swen30007.group5.assistedconnect.helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class RectangleView extends View {
	
	private Rect rect;
	private Paint paint;
	
    public RectangleView(Context context) {
        super(context);
        rect = new Rect();
        paint = new Paint();
    }
    
    @Override
	protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        rect.set(0, 0, canvas.getWidth(), canvas.getHeight());
        paint.setColor(Color.WHITE);
        canvas.drawRect(rect, paint);
    }
}

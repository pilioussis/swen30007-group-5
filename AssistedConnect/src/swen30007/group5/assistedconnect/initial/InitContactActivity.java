package swen30007.group5.assistedconnect.initial;

import java.util.ArrayList;
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.layout;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.adapters.AssistedContactAdapter;
import swen30007.group5.assistedconnect.assisted.ChangeContactsActivity;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

public class InitContactActivity extends Activity {
	final int PICK_CONTACT = 1234;
	ContactSQL db = new ContactSQL(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init_contact);
		
	}

//	public void selectContact(View view){
//		Intent intent = new Intent(this, InitDoneActivity.class);
//        startActivity(intent);
//	}
	
	//called when "pick contact" pressed in layout
		public void selectContact(View view) {
			//changes intent to android's inbuilt contact selector
			Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CONTACT);
		}
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
			//when returning from pick contact
			if(requestCode == PICK_CONTACT) {		
				//if the user presses back or cancels prematurely, stop
				if(resultCode != RESULT_CANCELED) {
					addContact(resultCode, data);				    					
				}
				else {
					//the user pressed back while in the android contact picker
				}			
			}	
		}
		
		private void addContact(int resultCode, Intent data)  {
			final Contact contact = new Contact(); // must be final for async dialog
			String id,hasPhone;
			int id_index, name_index, hasphone_index;
		
			//look up the result set returned by androids contact picker
			Uri contactUri = data.getData();
			Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
			
			//move to the first element of result, skips if null
			if (cursor.moveToFirst()) {
		
			    id_index = cursor.getColumnIndex(BaseColumns._ID);
			    id = cursor.getString(id_index);

			    name_index = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
			    //store name in contact class
			    contact.setName(cursor.getString(name_index));//-------------------------------------
			    				    
			    hasphone_index = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
			    hasPhone = cursor.getString(hasphone_index);
			   			    
			    getNumberFromContact(id, contact, hasPhone);			    
			}				
		}
		
		private void insertContactAndCheck(Contact contact) {
			boolean exists = db.checkIfContactExists(contact.getNumber(),true);

	    	if(!exists) {
	    		db.addContact(contact,true);
	    		contactAddSuccessfulDialog(contact.getName());


	    	}
	    	else {
	    		numberAlreadyExistsDialog();
	    	}
		}
		
		private void getNumberFromContact(String id, final Contact contact, String hasPhone) {
			ArrayList<String> numbers;
				
			//if a number is associated with that contact
			if(hasPhone.equals("1")) {		
				numbers = androidNumberTableLookup(id);
				if(numbers.size() == 1) {
					//set the primary number (the number of the phone with the app) keep other numbers in case??
					contact.setNumber(numbers.get(0));
					insertContactAndCheck(contact);	    	
				}
				else {
					manyNumberDialog(numbers, contact);
				}					
			}
			else {
				this.noNumberDialog();				
			}
		}
		
		private ArrayList<String> androidNumberTableLookup(String id) {
			ArrayList<String> numbers = new ArrayList<String>();
			ContentResolver cr = getContentResolver();
			//get all the number(s) associated with the contact
			Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				        			null,
				        			ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { Uri.encode(id) + "" },
				         			null);
			//add all the numbers to the contact class
			while (pCur.moveToNext()) {
				  String phoneNo =   pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				  numbers.add(phoneNo);
			}
			return numbers;
		}
		
		private void manyNumberDialog(ArrayList<String> numbers, final Contact contact) {
			//convert String array to final for use in other thread
			String[] numberArray = new String[numbers.size()];
		    numberArray = numbers.toArray(numberArray);						    
		    final String[] finalNumberArray = numberArray;
		    
			//run dialog for asking user to select primary 
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Select the number from this contact");					
			builder.setItems(numberArray, new DialogInterface.OnClickListener() {
			    @Override
				public void onClick(DialogInterface dialog, int item) {
			    	//add number then add to the database
			    	contact.setNumber(finalNumberArray[item]);
			    	insertContactAndCheck(contact);            
			    }
			});
			AlertDialog alert = builder.create();
			alert.show();				
		}
		
		private void noNumberDialog() {
			//if the contact was not associated with any number
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle("Sorry")
	        .setMessage("The contact you chose had no phone numbers.")
	        .setCancelable(false)
	        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
		
		private void numberAlreadyExistsDialog() {
			//if the contact was not associated with any number
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle("Sorry")
	        .setMessage("This number is already in your contact list")
	        .setCancelable(false)
	        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
		
		private void contactAddSuccessfulDialog(String contactName) {
			//if the contact was not associated with any number
			final Intent intent = new Intent(this, InitDoneActivity.class);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle("Success!")
	        .setMessage(contactName +  " has been added to your list of carers")
	        .setCancelable(false)
	        .setNegativeButton("OK",new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
		    		
		            startActivity(intent);
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
}

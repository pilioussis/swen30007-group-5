package swen30007.group5.assistedconnect.initial;

import swen30007.group5.assistedconnect.assisted.AssistedHomeActivity;
import swen30007.group5.assistedconnect.carer.CarerHomeActivity;
import swen30007.group5.assistedconnect.helpers.BaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;


public class InitialActivity extends BaseActivity {
	
	private boolean isCarer;
	Intent intent;
	@Override
    protected void onCreate(Bundle savedInstanceState) {   	
        super.onCreate(savedInstanceState);
        
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        isCarer = prefs.getBoolean("user_type", false);
        boolean initLaunch = prefs.getBoolean("initial_launch", true);

        if (!initLaunch) {
            if (isCarer){
            	intent = new Intent(this, CarerHomeActivity.class);
            }
            else {
            	intent = new Intent(this, AssistedHomeActivity.class);            
            }
        }
        else {
        	intent = new Intent(this, InitNameActivity.class);
        }   
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}

package swen30007.group5.assistedconnect.initial;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.helpers.UserDetails;
import swen30007.group5.assistedconnect.messaging.PopUp;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class InitNameActivity extends Activity {

	private EditText mainText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init_name);

		// Get views
		mainText = (EditText) findViewById(R.id.written_message);

		// Get custom type face
		Typeface typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");

		// Set Elements to the custom typeface
		// Force keyboard visible
		if (mainText.requestFocus()) {
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		}
		mainText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionID, KeyEvent event) {
				boolean handled = false;
				if (actionID == EditorInfo.IME_ACTION_DONE) {
		            onOK(v);
		            handled = true;
		        }
		        return handled;
			}
		});
	}
	
	
	/** Called when the user clicks the ok button */
	public void onOK(View view) {
		String name = mainText.getText().toString();
		if (name.length() < 1) {
			new PopUp(this, "Please enter in your name and try again.");
			return;
		}
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("username", name);
		editor.commit();
		launchNextIntent();
	}

	public void launchNextIntent() {
		Intent intent;
		// Change InitDoneActivity to next setting. Eventually will branch.
		// Move InitDoneActivity on to last setting
		if(UserDetails.attempAutoNumberSet(this)){
			intent = new Intent(this, InitModeActivity.class);
		}
		else{
			intent = new Intent(this, InitNumberActivity.class);
		}
		startActivity(intent);
	}

}

package swen30007.group5.assistedconnect.initial;

import swen30007.group5.assistedconnect.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

public class InitModeActivity extends Activity {

	private Button apButton;
	private Button carerButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init_mode);

		apButton = (Button) findViewById(R.id.ap_button);
		carerButton = (Button) findViewById(R.id.carer_button);

		// Get typeface
		Typeface typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");

		// Set typeface on views
		apButton.setTypeface(typeFace);
		carerButton.setTypeface(typeFace);

	}

	/** Called when the user clicks the Send Message button */
	public void carerMode(View view) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("user_type", true);
		editor.commit();
		launchNextIntent();
	}

	/** Called when the user clicks the Send Message button */
	public void apMode(View view) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("user_type", false);
		editor.commit();
		launchNextIntent();
	}

	public void launchNextIntent(){
    	Intent intent;
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isCarer = prefs.getBoolean("user_type", false);
    	if (isCarer){
    		intent = new Intent(this, InitDoneActivity.class);
    	}
    	else{
    		intent = new Intent(this, InitTimeActivity.class);
    	}
        startActivity(intent);
    }
}

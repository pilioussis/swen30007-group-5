package swen30007.group5.assistedconnect.initial;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.R.layout;
import swen30007.group5.assistedconnect.R.menu;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;


public class InitTimeActivity extends Activity {
	private Button textButton;
	private Button okButton;
	private TimePicker timePicker;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init_time);
		
		timePicker = (TimePicker) findViewById(R.id.time_picker);
		okButton = (Button) findViewById(R.id.ok_button);
		textButton = (Button) findViewById(R.id.text_button);

		// Get typeface
		Typeface typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");

		// Set typeface on views
		okButton.setTypeface(typeFace);
		textButton.setTypeface(typeFace);
		
	}

	/** Called when the user clicks the Send Message button */
	public void onOK(View view) {
		 String time = timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute();

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("message_reminder_time", time);	
		editor.commit();
		launchNextIntent();
	}
	public void launchNextIntent(){
    	Intent intent = new Intent(this, InitContactActivity.class);
        startActivity(intent);
    }


}

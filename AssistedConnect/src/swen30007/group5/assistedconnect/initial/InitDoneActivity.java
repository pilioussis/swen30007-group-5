package swen30007.group5.assistedconnect.initial;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.messaging.GcmMessenger;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

public class InitDoneActivity extends Activity {
	private Button okButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init_done);
		okButton = (Button)findViewById(R.id.ok_button);
		//initiate messaging service to register with server
		GcmMessenger gcmMessenger = new GcmMessenger(this);
		//Get typeface
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Thin.ttf");
        
        //Set typeface on views
        okButton.setTypeface(typeFace);
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor  = prefs.edit();
        editor.putBoolean("initial_launch", false);
        editor.commit();
        
	}
	
	//when screen is tapped
	public void onOK(View view){
		Intent intent = new Intent(this, InitialActivity.class);
        startActivity(intent);
	}


}

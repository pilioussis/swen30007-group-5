package swen30007.group5.assistedconnect.reminders;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class InactiveAlertService extends Service{

    private static ArrayList<InactiveAlertBroadcastReceiver> alarms = new ArrayList<InactiveAlertBroadcastReceiver>();
    
    //Variables for testing/checking lifecycle events
    protected int isCreated = 0;
    protected int isRunning = 0;
    
    @Override
	public void onCreate()
    {
        super.onCreate();       
        isCreated += 1;
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	isRunning += 1;
    	
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	boolean isCarer = prefs.getBoolean("user_type", false);
    	
    	if (isCarer){
    		boolean updateAll = intent.getBooleanExtra("update_all", false);
        	
        	if (updateAll){
    	    	ArrayList<String> assistedNumbers = intent.getStringArrayListExtra("assisted_numbers");
    	    	boolean found;
    	    	for (String assistedNumber : assistedNumbers){
    	    		found = false;
    	    		for (InactiveAlertBroadcastReceiver alarm : alarms){
    	    			if (alarm.getAssistedNumber() == assistedNumber){
    	            		alarm.SetAlarm(this, assistedNumber);
    	            		found = true;
    	            	}
    	    		}
    	    		if (!found){
    	    			InactiveAlertBroadcastReceiver newAlarm = new InactiveAlertBroadcastReceiver();
    	    	        newAlarm.SetAlarm(this, assistedNumber);
    	    	        alarms.add(newAlarm);
    	    		}
    	    	}
        	}
        	else {
            	String assistedNumber = intent.getStringExtra("assisted_number");
            	
            	boolean found = false;
                for (InactiveAlertBroadcastReceiver alarm : alarms){
                	if (alarm.getAssistedNumber() == assistedNumber){;
                		alarm.SetAlarm(this, assistedNumber);
                		found = true;
                		break;
                	}
                }
                
                if (!found){
    				InactiveAlertBroadcastReceiver newAlarm = new InactiveAlertBroadcastReceiver();
    				newAlarm.SetAlarm(this, assistedNumber);
    				alarms.add(newAlarm);
                }
        	}
    	}
    	
        return START_STICKY;
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	isCreated = 0;
    	isRunning = 0;
    	for (InactiveAlertBroadcastReceiver alarm : alarms){
    		alarm.CancelAlarm(this);
    	}
    	alarms.clear();
    }

    @Override
    public IBinder onBind(Intent intent) 
    {
        return null;
    }
}

package swen30007.group5.assistedconnect.reminders;

import java.util.Calendar;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.SQL.Message;
import swen30007.group5.assistedconnect.SQL.MessageSQL;
import swen30007.group5.assistedconnect.carer.CarerConversationActivity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

public class InactiveAlertBroadcastReceiver extends BroadcastReceiver {
	
	public static final int NOTIFICATION_ID = 3;
    private NotificationManager mNotificationManager; 
    NotificationCompat.Builder builder;
    private String assistedNumber = "";
    
    //Variables for testing/checking events
    protected boolean isStarted = false;
	
	public String getAssistedNumber() {
		return assistedNumber;
	}

	public void setAssistedNumber(String assistedNumber) {
		this.assistedNumber = assistedNumber;
	}

	@Override
	public void onReceive(Context context, Intent intent) 
	{   
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();

		//Pops the notification
		sendInactiveAlertNotification(context, intent.getStringExtra("assistedNumber"));
	
		wl.release();
	}

	/** Sets the alarm to remind assisted users to send a message */
	public void SetAlarm(Context context, String phno)
	{
		isStarted = true;
		setAssistedNumber(phno);
		
		MessageSQL messageDb = new MessageSQL(context);
		Calendar calendar = Calendar.getInstance();
		if (messageDb.getMessagesFromContact(phno).size() > 0){
			Message lastMessage = messageDb.getNewestMessage(phno);
			String lastMessageDate = lastMessage.getDate();
			
			//Gets the time of the last message for this contact in milliseconds			
			calendar.set(Integer.parseInt(lastMessageDate.substring(0, 4)), 
						 Integer.parseInt(lastMessageDate.substring(5,7)) - 1, 
						 Integer.parseInt(lastMessageDate.substring(8,10)), 
						 Integer.parseInt(lastMessageDate.substring(11, 13)), 
						 Integer.parseInt(lastMessageDate.substring(14, 16)), 
						 Integer.parseInt(lastMessageDate.substring(17, 19)));	
			calendar.add(Calendar.HOUR, calendar.get(Calendar.ZONE_OFFSET) / (1000*60*60) + 1);
		}
		long lastMessageTime = calendar.getTimeInMillis();
		       
        //Set up alarm manager and broadcast
		AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, InactiveAlertBroadcastReceiver.class);
		i.putExtra("assistedNumber", phno);
		PendingIntent pi;
		if (phno.length() < 12){
			pi = PendingIntent.getBroadcast(context, Integer.parseInt(phno.substring(2, phno.length())), i, 0);
		}
		else {
			pi = PendingIntent.getBroadcast(context, Integer.parseInt(phno.substring(2, 12)), i, 0);
		}
		
		//sets the alarm for 24 hours since last message time
		//repeat every hour
		am.setRepeating(AlarmManager.RTC_WAKEUP, 
						lastMessageTime + 1000*60*60*24, 
						1000*60*60, 
						pi);
	}

	/** Cancels the alarm (shoudn't need to be called ever) */
	public void CancelAlarm(Context context)
	{
		Intent intent = new Intent(context, InactiveAlertBroadcastReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, Integer.parseInt(getAssistedNumber().substring(2, 12)), intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}
	
	/** Pops a notification with default vibrate/sound/lights */
	private void sendInactiveAlertNotification(Context context, String phno){
		ContactSQL contactDb = new ContactSQL(context);
		Contact contact = contactDb.getContact(phno, true);		
		
    	mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(context, CarerConversationActivity.class);
        notificationIntent.putExtra("number", contact.getNumber());
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
        		notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("Assisted Connect")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(contact.getName() + " hasn't sent a message in 24 hours."))
        .setContentText(contact.getName() + " hasn't sent a message in 24 hours.")
        .setAutoCancel(true)
        .setDefaults(Notification.DEFAULT_ALL);
        
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(Integer.parseInt(phno.substring(2, 12)), mBuilder.build());
	}
}

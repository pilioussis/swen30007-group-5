package swen30007.group5.assistedconnect.reminders;

import java.util.ArrayList;

import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AutoStart extends BroadcastReceiver
{   
    @Override
    public void onReceive(Context context, Intent intent)
    {   
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
        	//Start reminder service
            context.startService(new Intent(context, MessageReminderService.class));
            
            //Start inactive alert services for all contacts
        	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        	boolean isCarer = prefs.getBoolean("user_type", false);
        	
        	if (isCarer){
                ContactSQL contactDb = new ContactSQL(context);
                Intent i = new Intent(context, InactiveAlertService.class);
                ArrayList<String> assistedNumbers = new ArrayList<String>();
                for(Contact contact : contactDb.getAllContacts(true)){  	
                	assistedNumbers.add(contact.getNumber());
                }
                i.putExtra("assisted_numbers", assistedNumbers);
                i.putExtra("update_all", true);
                context.startService(intent);
        	}
        }
    }
}
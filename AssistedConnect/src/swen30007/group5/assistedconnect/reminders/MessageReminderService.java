package swen30007.group5.assistedconnect.reminders;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MessageReminderService extends Service
{
    MessageReminderBroadcastReceiver alarm = new MessageReminderBroadcastReceiver();
    
    //Variables for testing/checking lifecycle events
    protected int isCreated = 0;
    protected int isRunning = 0;
    
    @Override
	public void onCreate()
    {
        super.onCreate();       
        isCreated += 1;
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        alarm.SetAlarm(this);
        isRunning += 1;
        
        return START_STICKY;
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	isCreated = 0;
    	isRunning = 0;
    }

    @Override
    public IBinder onBind(Intent intent) 
    {
        return null;
    }
}
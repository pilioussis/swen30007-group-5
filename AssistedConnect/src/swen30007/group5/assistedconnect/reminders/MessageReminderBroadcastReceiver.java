package swen30007.group5.assistedconnect.reminders;

import java.util.Calendar;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.assisted.SelectRecipientsActivity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class MessageReminderBroadcastReceiver extends BroadcastReceiver 
{    
    public static final int NOTIFICATION_ID = 2;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    
    //Variables for testing/checking events
    protected boolean isStarted = false;
	
	@Override
	public void onReceive(Context context, Intent intent) 
	{   
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();

		//Pops the notification
		sendReminderNotification(context);
	
		wl.release();
	}

	/** Sets the alarm to remind assisted users to send a message */
	public void SetAlarm(Context context)
	{
		isStarted = true;
		
		//Get saved settings for reminder time
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String[] reminderTime = prefs.getString("message_reminder_time", 
        										context.getString(R.string.message_reminder_time_default))
        										.split(":");
        
        //Set up alarm manager and broadcast
		AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, MessageReminderBroadcastReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		
		//sets the alarm repeating every 24 hours
		am.setRepeating(AlarmManager.RTC_WAKEUP, 
						getStartTime(Integer.parseInt(reminderTime[0]), Integer.parseInt(reminderTime[1])), 
						1000*60*60*24, 
						pi);
	}

	/** Cancels the alarm (shoudn't need to be called ever) */
	public void CancelAlarm(Context context)
	{
		Intent intent = new Intent(context, MessageReminderBroadcastReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}
	
	/** Pops a notification with default vibrate/sound/lights */
	private void sendReminderNotification(Context context){
    	mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    	        
        Intent notificationIntent = new Intent(context, SelectRecipientsActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
        		notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
        .setSmallIcon(R.drawable.notification_alarm)
        .setContentTitle("Assisted Connect")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText("Send a message to your loved ones!"))
        .setContentText("Send a message to your loved ones!")
        .setAutoCancel(true)
        .setDefaults(Notification.DEFAULT_ALL);
        
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
	
	/**
	 * Gets the time in milliseconds for when the alarm should be first set
	 * @param hours
	 * @param minutes
	 * @return time in milliseconds
	 */
	protected long getStartTime(int hours, int minutes) {
		//Current clock time
		Calendar clockCalendar = Calendar.getInstance();
		
		//Gets the time right now in milliseconds
		Calendar calendar = Calendar.getInstance();
		calendar.set(clockCalendar.get(Calendar.YEAR), clockCalendar.get(Calendar.MONTH), clockCalendar.get(Calendar.DAY_OF_MONTH), 
		             hours, minutes, 0);
		long startTime = calendar.getTimeInMillis();
		
		//adjusts the time to the next day if already occurred
		if (clockCalendar.getTimeInMillis() > startTime){
			startTime += 1000*60*60*24;
		}
		
		return startTime;
	}
}
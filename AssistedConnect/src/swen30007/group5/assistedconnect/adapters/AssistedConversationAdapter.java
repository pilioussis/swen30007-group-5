package swen30007.group5.assistedconnect.adapters;
 
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Message;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class AssistedConversationAdapter extends ArrayAdapter<Message> {
 
    Context context;
 
    public AssistedConversationAdapter(Context context, int resourceId, List<Message> items) {
        super(context, resourceId, items);

        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
        TextView txt;
    }
 
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Message curr = getItem(position);
        
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Thin.ttf");

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	if(curr.getIncoming() == 0) {     		
    		convertView = mInflater.inflate(R.layout.assisted_conversation_left_text, null);
    	}
    	else {
    		convertView = mInflater.inflate(R.layout.assisted_conversation_right_text, null);
    	}
        holder = new ViewHolder();
        holder.txt = (TextView) convertView.findViewById(R.id.text);
        
        holder.txt.setTypeface(typeFace);
        
        convertView.setTag(holder);

        holder.txt.setText(curr.getMessage());
        return convertView;
    }
}
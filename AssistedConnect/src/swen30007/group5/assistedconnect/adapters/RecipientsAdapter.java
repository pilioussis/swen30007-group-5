package swen30007.group5.assistedconnect.adapters;

import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import swen30007.group5.assistedconnect.helpers.SelectedRecipients;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RecipientsAdapter extends BaseAdapter {

        private Context mContext;
        private List<Contact> contacts;
        private final ContactSQL db;
        private final int maxLettersNameToDisplay = 7;

    public RecipientsAdapter(Context c) {
        mContext = c; 
        db = new ContactSQL(c);
        contacts = db.getAllContacts(true);
    }
        
        @Override
        public int getCount() {
                // TODO Auto-generated method stub
                return contacts.size() + 1;
        }

        @Override
        public Object getItem(int arg0) {
                // TODO Auto-generated method stub
                return null;
        }

        @Override
        public long getItemId(int arg0) {
                // TODO Auto-generated method stub
                return 0;
        }
        
        /*private view holder class*/
    private class ViewHolder {
        ImageView contactAvatar;
        TextView contactName;
        TextView contactNumber;
        View overlay;
    }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                Boolean lastItem = false;
                
                if (position >= contacts.size()){
                        lastItem = true;
                }
                
                ViewHolder holder = null;
                
                LayoutInflater mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                
        if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = mInflater.inflate(R.layout.recipients_item, null);
            holder = new ViewHolder();
            holder.contactName = (TextView) convertView.findViewById(R.id.contact_name);
            holder.contactAvatar = (ImageView) convertView.findViewById(R.id.avatar);
            holder.contactNumber = (TextView) convertView.findViewById(R.id.number);
            holder.overlay = convertView.findViewById(R.id.overlay);
            
            //Get custom type face   
            Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(),"fonts/Roboto-Thin.ttf");
            
            //Set Elements to the custom typeface
            holder.contactName.setTypeface(typeFace);        
            
            convertView.setTag(holder);
            

        } else {
                holder = (ViewHolder) convertView.getTag();
        }

        //If last item and thus the add contacts button
        if (lastItem){
                holder.contactName.setText("");
                holder.contactAvatar.setImageResource(R.drawable.ic_input_add);
                holder.contactNumber.setText("-1");
        }
        //Otherwise a contact grid item
        else {
                //Set the name and cut it off if too long for one line
                if (contacts.get(position).getName().length() <= maxLettersNameToDisplay){
                        holder.contactName.setText(contacts.get(position).getName());                        
                }
                else {
                        holder.contactName.setText(contacts.get(position).getName().subSequence(0, 
                                                                                        maxLettersNameToDisplay - 2) + "..");           
                }
                
                holder.contactAvatar.setImageResource(R.drawable.ic_contact_picture);
                holder.contactNumber.setText(contacts.get(position).getNumber());
                
                //Make overlay visible if the recipient has been selected previously
                if (SelectedRecipients.hasRecipient(contacts.get(position).getNumber())){
                        holder.overlay.setVisibility(View.VISIBLE);
                }
        }
        return convertView;
        }

}
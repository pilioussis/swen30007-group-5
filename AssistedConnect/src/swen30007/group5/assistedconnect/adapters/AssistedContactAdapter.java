package swen30007.group5.assistedconnect.adapters;
 
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import swen30007.group5.assistedconnect.SQL.ContactSQL;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class AssistedContactAdapter extends ArrayAdapter<Contact> {
	
    Context context;
    public AssistedContactAdapter(Context context, int resourceId, List<Contact> items) {
        super(context, resourceId, items);
        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
    	ImageView delete;
        TextView txt;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Contact curr = getItem(position);
        
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Thin.ttf");

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
      
        		
        	convertView = mInflater.inflate(R.layout.assisted_contact_row, null);

            holder = new ViewHolder();
            holder.txt = (TextView) convertView.findViewById(R.id.text);
            holder.delete = (ImageView) convertView.findViewById(R.id.next);
            
            holder.txt.setTypeface(typeFace);
            holder.txt.setTextSize(25);
            
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txt.setText(curr.getName());
        holder.delete.setImageResource(R.drawable.ic_delete);
        holder.delete.setOnClickListener( new View.OnClickListener() {
        	ContactSQL db = new ContactSQL(context);
        	int contactCount = db.getContactsCount(true);
			@Override
			public void onClick(View v) {
				if (contactCount>1){
					removeConfirmationDialog(curr);
				}
				else{ 
					cantRemoveLastContactDialog();					
				}
				
			}
		});
     
 
        return convertView;
    }
    
	private void removeConfirmationDialog(final Contact contact) {
		final ContactSQL db = new ContactSQL(context);
		AlertDialog dialog = new AlertDialog.Builder(context).create();
	    dialog.setTitle("Confirm Contact Removal");
	    dialog.setMessage("Are you sure you want to remove " + contact.getName() + " from your contact list?");
	    dialog.setCancelable(false);
	    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
	        @Override
			public void onClick(DialogInterface dialog, int buttonId) {

	            db.deleteContact(contact.getNumber(),true);
	            remove(contact);
	            notifyDataSetChanged();
	        }
	    });
	    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
	        @Override
			public void onClick(DialogInterface dialog, int buttonId) {
	            //return to activity
	        	dialog.cancel();
	        }
	    });
	    dialog.setIcon(android.R.drawable.ic_dialog_alert);
	    dialog.show();
    }
	
	private void cantRemoveLastContactDialog() {
		AlertDialog dialog = new AlertDialog.Builder(context).create();
	    dialog.setTitle("Contact Removal Denied.");
	    dialog.setMessage("Sorry, you can't have zero contacts. Please add a new contact before removing the last.");
	    dialog.setCancelable(false);
	    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
	        @Override
			public void onClick(DialogInterface dialog, int buttonId) {
	        	//return to activity
	        	dialog.cancel();
	        }
	    });
	    dialog.setIcon(android.R.drawable.ic_dialog_alert);
	    dialog.show();
    }
    
}
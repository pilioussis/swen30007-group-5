package swen30007.group5.assistedconnect.adapters;
 
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
public class CarerContactAdapter extends ArrayAdapter<Contact> {
 
    Context context;
    Intent intent;
    public CarerContactAdapter(Intent intent,Context context, int resourceId, List<Contact> items) {
        super(context, resourceId, items);
        this.intent = intent;
        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
    	ImageView next;
        TextView txt;
        RelativeLayout row;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Contact curr = getItem(position);
        
        //Get custom type face   
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Thin.ttf");

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
      
        		
        	convertView = mInflater.inflate(R.layout.carer_contact_row, null);

            holder = new ViewHolder();
            holder.txt = (TextView) convertView.findViewById(R.id.text);
            holder.next = (ImageView) convertView.findViewById(R.id.next);
            holder.row = (RelativeLayout) convertView.findViewById(R.id.even_container);
            
            holder.txt.setTypeface(typeFace);
            holder.txt.setTextSize(25);
            
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txt.setText(curr.getName());
        holder.next.setImageResource(R.drawable.emerald_next);
        holder.row.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent.putExtra("number", curr.getNumber());
				context.startActivity(intent);
				
			}
		});
     
 
        return convertView;
    }
    
    
}
package swen30007.group5.assistedconnect.adapters;
 
import java.util.List;

import swen30007.group5.assistedconnect.R;
import swen30007.group5.assistedconnect.SQL.Contact;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
public class AssistedContactConvoAdapter extends ArrayAdapter<Contact> {
 
    Context context;
    Intent intent;
    Typeface typeFace;
    
    public AssistedContactConvoAdapter(Intent intent,Context context, int resourceId, List<Contact> items) {
        super(context, resourceId, items);
        this.intent = intent;
        this.context = context;
      
        //Get custom type face   
        typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Thin.ttf");
    }
 
    /*private view holder class*/
    private class ViewHolder {
        TextView txt;
        RelativeLayout row;
    }

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Contact curr = getItem(position);
     
        
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
      
        		
        	convertView = mInflater.inflate(R.layout.assisted_contact_convo_row, null);

            holder = new ViewHolder();
            holder.txt = (TextView) convertView.findViewById(R.id.text);
            holder.row = (RelativeLayout) convertView.findViewById(R.id.even_container);
            
            holder.txt.setTypeface(typeFace);
            
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txt.setText(curr.getName());
        holder.row.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent.putExtra("number", curr.getNumber());
				context.startActivity(intent);
				
			}
		});
    
        return convertView;
    }
    
    
}
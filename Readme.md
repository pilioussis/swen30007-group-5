# SWEN30007 Group 5 Read me

## Overview.

This Android project is an Emergency Alarm created for the final semester project in the Bachelor of Science (Software Systems) at The University of Melbourne. It allows Assisted People to raise alarms to a specified group of carers in the case that they need help. It allows messages to be sent with a simple keyboard free interface.

## Building.


### Server
The server consists of two main components, `Server/AssistedDB/server.py' is the main python server file that polls GCM. It is contained in a larger Django project which contains an admin interface and storage for the GID numbers.


### AssistedConnect Android Project
The Android project in the `AssitedConnect/` directory is the client side app for the application, it can be building using the ADK. 

## Authors
- Dean Pilioussis
- Jordan Steele
- Geoffroy Cowan

## Screenshots
[Home Screen](https://bitbucket.org/pilioussis/swen30007-group-5/raw/40bd837a5464c15e100d497514e219dd894865dc/Docs/screenshots/Screenshot_2013-11-24-16-41-00.png)
[Voice to Text](https://bitbucket.org/pilioussis/swen30007-group-5/raw/40bd837a5464c15e100d497514e219dd894865dc/Docs/screenshots/Screenshot_2013-11-24-16-41-27.png)
[Conversation Screen](https://bitbucket.org/pilioussis/swen30007-group-5/raw/40bd837a5464c15e100d497514e219dd894865dc/Docs/screenshots/Screenshot_2013-11-24-16-41-40.png)
[Select Recipients](https://bitbucket.org/pilioussis/swen30007-group-5/raw/40bd837a5464c15e100d497514e219dd894865dc/Docs/screenshots/Screenshot_2013-11-24-16-43-02.png)

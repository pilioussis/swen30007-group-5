#!/bin/bash
until python server.py; do
    echo "'server.py' crashed with exit code $?. Restarting..." >&2
    sleep 1
done


from django.db import models
from django.contrib import admin
from datetime import datetime
class Contact(models.Model):
    phone = models.CharField(max_length=20, primary_key=True)
    GID  = models.CharField(max_length=1024)
    date = models.DateTimeField(default=datetime.now, blank=True)

    def __unicode__(self):
    	return "phone: " + self.phone + "  GID: " + self.GID

admin.site.register(Contact)

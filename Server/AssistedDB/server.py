#!/usr/bin/python
import sys, json, xmpp, random, string

#import statements for djangodb
from django.core.management import setup_environ
from AssistedDB import settings
setup_environ(settings)
from GIDlookup.models import Contact

SERVER = 'gcm.googleapis.com'
# GCM port as defined by google
PORT = 5235
#Google project ID number
USERNAME = "92439767333"
#API key
PASSWORD = "AIzaSyAwWdgiH3fJDEmlkcxKSCyAYJRrED2Zmu8"

unacked_messages_quota = 1000
send_queue = []

#MESSAGE ID STRINGS:
ALARM_MSG_ID = "alarm"
TEXT_MSG_ID = "message"
REGISTRATION_MSG_ID = "registration"
ALARM_CANCEL_MSG_ID = "cancel"
USER_CHECK_MSG_ID = "check_user"

# Return a random alphanumerical id
def random_id():
  rid = ''
  for x in range(8): rid += random.choice(string.ascii_letters + string.digits)
  return rid

# Function processes incoming messages
def message_callback(session, message):
  global unacked_messages_quota
  gcm = message.getTags('gcm')
  if gcm:
    gcm_json = gcm[0].getData()
    msg = json.loads(gcm_json)
    #if message is from user (not auto ack/nack msg)
    if not msg.has_key('message_type'):
      # Acknowledge the incoming message immediately.
      send({'to': msg['from'],
            'message_type': 'ack',
            'message_id': msg['message_id']})
      # Queue a response back to the server.
      if msg.has_key('from') and msg.has_key('data'):
        if msg['data'].has_key('MSG_ID'):
          #if message is of alarm type- send alarm to recipientss
          if msg['data']['MSG_ID'] == ALARM_MSG_ID:
            send_alarm(msg['from'], msg['data'])
          #if message is of text type- send text to recipients
          elif msg['data']['MSG_ID'] == TEXT_MSG_ID:
              send_text_message(msg['from'], msg['data'])
          #if message is of store reg. type- store gcmID/ph# combination
          elif msg['data']['MSG_ID'] == REGISTRATION_MSG_ID: 
              store_phone_number(msg['from'], msg['data']['PHNO'])  
          #if message is of check user type return whether ph.no is stored
          elif msg['data']['MSG_ID'] == USER_CHECK_MSG_ID: 
            check_user(msg['from'], msg['data']['PHNO'])  
    elif msg['message_type'] == 'ack' or msg['message_type'] == 'nack':
      unacked_messages_quota += 1

#returns 
def check_user(return_id, phone_number):
  if(get_gcm_id(phone_number)):
    result = "true"
  else:
    result = "false" 
  send_queue.append({'to': return_id, 'message_id': random_id(), 
    'data': {'MSG_ID': USER_CHECK_MSG_ID,  'result': result}})

#forwards alarm to all recipients as described in data bundle
def send_alarm(sender_id, data):
  recipients = []
  from_number = get_phone_number(sender_id)
  if(from_number):
    print "Alarm sent from: " + from_number
  #get all recipient phone numbers from data bundle
  for key in data.keys():
    if "RECIPIENT." in key:
      recipient = get_gcm_id(data[key])
      if recipient == False:
        #TODO: error handle
        print "ERROR: Couldnt find gcm_id in database for ph#: "+data[key]
      else:
        recipients.append(recipient)
  #Send alarm to each recipient in carer list
  for recipient in recipients:
    print "Alarm sent to: " + get_phone_number(recipient)
    send_queue.append({'to': recipient, 'message_id': random_id(), 
      'data': {'MSG_ID': ALARM_MSG_ID, 'name': data['name'], 
      'from_number': from_number,'location': data['location'],
      'timestamp': data['timestamp']}})

#Forwards a standard text message to recipients as described in data bundle
def send_text_message(sender_id, data):
  recipients = []
  from_number = get_phone_number(sender_id)
  #get all recipient phone numbers from data bundle
  for key in data.keys():
    if "RECIPIENT." in key:
      recipient = get_gcm_id(data[key])
      if recipient == False:
        #TODO: error handle
        print "ERROR: Couldnt find gcm_id in database for ph#: "+data[key]
      else:
        recipients.append(recipient)
  #Send message to each recipient in carer list
  for recipient in recipients:
    print "message sent: " + data['message']
    send_queue.append({'to': recipient, 'message_id': random_id(), 
      'data': {'MSG_ID': TEXT_MSG_ID, 'name': data['name'], 
      'from_number': from_number, 'location': data['location'], 
      'timestamp': data['timestamp'], 'message':data['message']}})

def send(json_dict):
  template = ("<message><gcm xmlns='google:mobile:data'>{1}</gcm></message>")
  client.send(xmpp.protocol.Message(
      node=template.format(client.Bind.bound[0], json.dumps(json_dict))))

def flush_queued_messages():
  global unacked_messages_quota
  while len(send_queue) and unacked_messages_quota > 0:
    send(send_queue.pop(0))
    unacked_messages_quota -= 1



#Returns a GCM ID from user database given a phone number
def get_gcm_id(phone_number):
  try:
    c = Contact.objects.get(phone=phone_number)
    return c.GID
  except:
    return False

#Stores a users phone number/gcm_id pair in user database
def store_phone_number(gcm_id, phone_number):
  print "registered phone number :" + phone_number
  if (not(get_gcm_id(phone_number))):
    c = Contact(phone = phone_number , GID = gcm_id)
    c.save()
  return
 
#Returns a phone number from user database given a GCM ID
def get_phone_number(gcm_id):
  try:
    print gcm_id
    c = Contact.objects.filter(GID=gcm_id)
    if len(c) >= 1:
      return c.order_by('-pk')[0].phone
    else:
      print "gcm_id not found"
  except:
      print "database error"
  return False

#Initialize connection to GCM server
client = xmpp.Client('gcm.googleapis.com', debug=[""]);
client.connect(server=(SERVER,PORT), secure=1, use_srv=False)
auth = client.auth(USERNAME, PASSWORD)
if not auth:
  print 'Authentication failed!'
  sys.exit(1)
client.RegisterHandler('message', message_callback)

#Main server loop
print "Server ready"
while True:
  client.Process(1)
  flush_queued_messages()
